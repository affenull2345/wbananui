/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <bananui/utils.h>
void bRegisterEventHandler(bEventHandler **list, bEventCallback cb,
	void *userdata)
{
	bEventHandler *tmp;
	tmp = malloc(sizeof(bEventHandler));
	tmp->next = *list;
	tmp->cb = cb;
	tmp->userdata = userdata;
	*list = tmp;
}

void bUnregisterEventHandler(bEventHandler **list, bEventCallback cb,
	void *userdata)
{
	for(; *list; list = &((*list)->next)){
		if((*list)->cb == cb && (*list)->userdata == userdata){
			bEventHandler *tmp;
			tmp = (*list)->next;
			free(*list);
			*list = tmp;
			return;
		}
	}
}

int bTriggerEvent(bEventHandler *list, void *param)
{
	for(; list; list = list->next){
		if(!list->cb(param, list->userdata)){
			return 0;
		}
	}
	return 1;
}

