/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <assert.h>
#include <wchar.h>
#include <stdlib.h>
#include <bananui/window.h>
#include <bananui/widget.h>
#include <bananui/highlevel.h>
#include <xkbcommon/xkbcommon.h>
#include "text-input-client-protocol.h"

#define ICON_WIDTH 48
#define BUTTON_PADDING 5

static int focus_handler(void *param, void *data)
{
	bButtonWidget *btn = data;
	btn->box->color = btn->selected_bg_color;
	btn->cont->color = btn->selected_fg_color;
	btn->subtitle->color = btn->selected_fg_color;
	if(btn->icon) btn->icon->color = btn->selected_fg_color;
	btn->focused = 1;
	return 1;
}

static int unfocus_handler(void *param, void *data)
{
	bButtonWidget *btn = data;
	btn->box->color = btn->default_bg_color;
	btn->cont->color = btn->default_fg_color;
	btn->subtitle->color = btn->default_fg_color;
	if(btn->icon) btn->icon->color = btn->default_fg_color;
	btn->focused = 0;
	return 1;
}

bButtonWidget *bCreateButtonWidget(const char *text, bButtonStyle st,
		bContentAlign align, int width, int height)
{
	bButtonWidget *btn;
	btn = malloc(sizeof(bButtonWidget));
	if(!btn) return NULL;
	btn->iconbox = NULL;
	btn->icon = NULL;
	btn->state = 0;
	btn->style = st;
	btn->box = bCreateBoxWidget(align, B_BOX_HBOX, width, height);
	if(!btn->box){
		free(btn);
		return NULL;
	}
	btn->box->focusable = 1;
	if(st == B_BUTTON_STYLE_ICON){
		btn->iconbox = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_VBOX,
			ICON_WIDTH > width ? ICON_WIDTH : width, 0);
		if(!btn->iconbox){
			bDestroyBoxRecursive(btn->box);
			free(btn);
			return NULL;
		}
		bAddBoxToBox(btn->box, btn->iconbox);
		btn->icon = bCreateContentWidget(B_ALIGN_CENTER, 0);
		if(!btn->icon){
			bDestroyBoxRecursive(btn->box);
			free(btn);
			return NULL;
		}
		bAddContentToBox(btn->iconbox, btn->icon);
	}
	btn->subbox = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_VBOX, -1, 0);
	if(!btn->subbox){
		bDestroyBoxRecursive(btn->box);
		free(btn);
		return NULL;
	}
	if(st == B_BUTTON_STYLE_ICON){
		btn->subbox->mgn_r = BUTTON_PADDING;
	}
	else if(st != B_BUTTON_STYLE_PUSHBUTTON){
		btn->subbox->mgn_l = BUTTON_PADDING;
	}
	bAddBoxToBox(btn->box, btn->subbox);
	btn->cont = bCreateContentWidget(
		st == B_BUTTON_STYLE_PUSHBUTTON ?
		B_ALIGN_CENTER : B_ALIGN_START, 0);
	if(!btn->cont){
		bDestroyBoxRecursive(btn->box);
		free(btn);
		return NULL;
	}
	bSetTextContent(btn->cont, text, PANGO_WEIGHT_NORMAL, 16);
	btn->cont->wrapmode = B_WRAP_ELLIPSIZE;
	bAddContentToBox(btn->subbox, btn->cont);
	btn->subtitle = bCreateContentWidget(
		st == B_BUTTON_STYLE_PUSHBUTTON ? B_ALIGN_CENTER : B_ALIGN_START, 0);
	if(!btn->subtitle){
		bDestroyBoxRecursive(btn->box);
		free(btn);
		return NULL;
	}
	bAddContentToBox(btn->subbox, btn->subtitle);
	if(st == B_BUTTON_STYLE_RADIO || st == B_BUTTON_STYLE_CHECKBOX){
		btn->iconbox = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_VBOX,
			ICON_WIDTH > width ? ICON_WIDTH : width, 0);
		if(!btn->iconbox){
			bDestroyBoxRecursive(btn->box);
			free(btn);
			return NULL;
		}
		bAddBoxToBox(btn->box, btn->iconbox);
		btn->icon = bCreateContentWidget(B_ALIGN_CENTER, 0);
		if(!btn->icon){
			bDestroyBoxRecursive(btn->box);
			free(btn);
			return NULL;
		}
		if(st == B_BUTTON_STYLE_RADIO){
			bLoadImageFromIcon(btn->icon, "radio-symbolic", 24);
		}
		else if(st == B_BUTTON_STYLE_CHECKBOX){
			bLoadImageFromIcon(btn->icon, "checkbox-symbolic", 24);
		}
		bAddContentToBox(btn->iconbox, btn->icon);
	}
	btn->default_bg_color = bColorFromTheme("button:background");
	btn->default_fg_color = bColorFromTheme("button:text");
	btn->selected_bg_color = bColorFromTheme("button:selected:background");
	btn->selected_fg_color = bColorFromTheme("button:selected:text");
	btn->focused = 0;
	btn->box->color = btn->default_bg_color;
	btn->cont->color = btn->default_fg_color;
	btn->subtitle->color = btn->default_fg_color;
	bRegisterEventHandler(&btn->box->focusin, focus_handler, btn);
	bRegisterEventHandler(&btn->box->focusout, unfocus_handler, btn);
	return btn;
}

void bSetButtonState(bButtonWidget *btn, int state)
{
	if(btn->state == state) return;
	btn->state = state;
	if(btn->style == B_BUTTON_STYLE_RADIO && state == 0){
		bLoadImageFromIcon(btn->icon, "radio-symbolic", 24);
	}
	else if(btn->style == B_BUTTON_STYLE_RADIO && state == 1){
		bLoadImageFromIcon(btn->icon, "radio-checked-symbolic", 24);
	}
	else if(btn->style == B_BUTTON_STYLE_CHECKBOX && state == 0){
		bLoadImageFromIcon(btn->icon, "checkbox-symbolic", 24);
	}
	else if(btn->style == B_BUTTON_STYLE_CHECKBOX && state == 1){
		bLoadImageFromIcon(btn->icon, "checkbox-checked-symbolic", 24);
	}
}
void bSetButtonDefaultColor(bButtonWidget *btn,
	bWidgetColor fg, bWidgetColor bg)
{
	btn->default_fg_color = fg;
	btn->default_bg_color = bg;
	if(!btn->focused){
		btn->box->color = btn->default_bg_color;
		btn->cont->color = btn->default_fg_color;
		btn->subtitle->color = btn->default_fg_color;
		if(btn->icon) btn->icon->color = btn->default_fg_color;
	}
}
void bSetButtonSelectedColor(bButtonWidget *btn,
	bWidgetColor fg, bWidgetColor bg)
{
	btn->selected_fg_color = fg;
	btn->selected_bg_color = bg;
	if(btn->focused){
		btn->box->color = btn->selected_bg_color;
		btn->cont->color = btn->selected_fg_color;
		btn->subtitle->color = btn->selected_fg_color;
		if(btn->icon) btn->icon->color = btn->selected_fg_color;
	}
}

static int inp_handle_update(void *param, void *data);

static void inp_render(bInputWidget *inp)
{
	size_t tmp_mbs_size, text_size, preedit_size = 0;
	char *tmp_mbs;
	wchar_t *tmp_wcs;
	/* Insert preedit string into DISPLAYED input */

	text_size = wcslen(inp->text);
	if(inp->wnd->tis.preedit){
		preedit_size = mbstowcs(NULL, inp->wnd->tis.preedit, 0);
		if(preedit_size == (size_t)-1) preedit_size = 0;
	}
	tmp_wcs = malloc((preedit_size + text_size + 1) * sizeof(wchar_t));
	if(tmp_wcs){
		wchar_t at_cursor;
		int cursor_bytes;
		int i;
		if(preedit_size > 0){
			mbstowcs(tmp_wcs+inp->cursor, inp->wnd->tis.preedit, 
				preedit_size);
		}
		for(i = text_size; i >= 0; i--){
			if(i >= inp->cursor){
				tmp_wcs[i + preedit_size] = inp->text[i];
			}
			else {
				tmp_wcs[i] = inp->text[i];
			}
		}
		at_cursor = tmp_wcs[inp->cursor + preedit_size];
		tmp_wcs[inp->cursor + preedit_size] = L'\0';
		cursor_bytes = wcstombs(NULL, tmp_wcs, 0);
		tmp_wcs[inp->cursor + preedit_size] = at_cursor;
		tmp_mbs_size = wcstombs(NULL, tmp_wcs, 0);
		tmp_mbs = malloc(tmp_mbs_size+1);
		if(tmp_mbs){
			wcstombs(tmp_mbs, tmp_wcs, tmp_mbs_size);
			tmp_mbs[tmp_mbs_size] = '\0';
			bSetTextContent(inp->cont, tmp_mbs,
				PANGO_WEIGHT_NORMAL, 16);
			inp->cont->cursor = cursor_bytes;
			inp->cont->cursor_visible = inp->active;
			bRedrawWindow(inp->wnd);
			free(tmp_mbs);
		}
		free(tmp_wcs);
	}
}

static void inp_ime_enable(bInputWidget *inp)
{
	if(!inp->wnd->text_input) return;
#if BANANUI_VERBOSE
	fprintf(stderr, "[DEBUG] Enable IME\n");
#endif
	zwp_text_input_v3_enable(inp->wnd->text_input);
	zwp_text_input_v3_set_content_type(
		inp->wnd->text_input,
		ZWP_TEXT_INPUT_V3_CONTENT_HINT_NONE,
		inp->mode == B_INPUT_MODE_NUMBER ?
		ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_NUMBER :
		ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_ALPHA);

	zwp_text_input_v3_commit(inp->wnd->text_input);
	inp->active = 1;
	inp_render(inp);
}

static void inp_ime_disable(bInputWidget *inp)
{
	if(!inp->wnd->text_input) return;
#if BANANUI_VERBOSE
	fprintf(stderr, "[DEBUG] Disable IME\n");
#endif
	zwp_text_input_v3_disable(inp->wnd->text_input);
	zwp_text_input_v3_commit(inp->wnd->text_input);
	inp->active = 0;
	inp_render(inp);
}

static int inp_handle_leave(void *param, void *data)
{
	bInputWidget *inp = data;
	assert(inp->wnd->text_input);
	assert(inp->focused);
	inp_ime_disable(inp);
	return 1;
}
static int inp_handle_enter(void *param, void *data)
{
	bInputWidget *inp = data;
	assert(inp->wnd->text_input);
	assert(inp->focused);
	inp_ime_enable(inp);
	return 1;
}

static int inp_handle_keydown(void *param, void *data)
{
	bInputWidget *inp = data;
	xkb_keysym_t *sym = param;
	if(*sym == XKB_KEY_BackSpace && inp->cursor > 0){
		int i, textlen = wcslen(inp->text);
		inp->cursor--;
#if BANANUI_VERBOSE
		fprintf(stderr, "[DEBUG] Input delete\n");
#endif
		for(i = inp->cursor; i < textlen; i++){
			inp->text[i] = inp->text[i+1];
		}
		inp_render(inp);
		return 0;
	}
	else if(*sym == XKB_KEY_Left && inp->cursor > 0){
		inp->cursor--;
#if BANANUI_VERBOSE
		fprintf(stderr, "[DEBUG] Input cursor left\n");
#endif
		inp_render(inp);
		return 0;
	}
	else if(*sym == XKB_KEY_Right && inp->text[inp->cursor]){
		inp->cursor++;
#if BANANUI_VERBOSE
		fprintf(stderr, "[DEBUG] Input cursor right\n");
#endif
		inp_render(inp);
		return 0;
	}
	return 1;
}
static int inp_handle_keyup(void *param, void *data)
{
	bInputWidget *inp = data;
	xkb_keysym_t *sym = param;
	return 1;
}


static int inp_handle_update(void *param, void *data)
{
	bInputWidget *inp = data;
	assert(inp->wnd->text_input);
	assert(inp->focused);

	/* Insert commit string into text input */

#if BANANUI_VERBOSE
	fprintf(stderr, "[DEBUG] IME update: \n");
#endif
	if(inp->wnd->tis.commit_string){
		size_t wchars;
#if BANANUI_VERBOSE
		fprintf(stderr, "[DEBUG]  commit: %s\n",
			inp->wnd->tis.commit_string);
#endif
		wchars = mbstowcs(NULL, inp->wnd->tis.commit_string, 0);
		if(wchars != 0 || wchars != (size_t)-1){
			int i, textlen = wcslen(inp->text);
			while(textlen + wchars >= inp->bufsize){
				inp->text = realloc(inp->text,
					inp->bufsize * 2 * sizeof(wchar_t));
				inp->bufsize = inp->bufsize*2;
			}
			for(i = textlen; i >= inp->cursor; i--){
				inp->text[i + wchars] = inp->text[i];
			}
			mbstowcs(inp->text+inp->cursor,
				inp->wnd->tis.commit_string,
				wchars);
			inp->cursor += wchars;
		}
	}
	inp_render(inp);
	return 1;
}

static int inp_focus_handler(void *param, void *data)
{
	bInputWidget *inp = data;
	inp->box->color = inp->selected_bg_color;
	inp->focused = 1;
	inp_ime_enable(inp);
	bRegisterEventHandler(&inp->wnd->text_input_update,
		inp_handle_update, inp);
	bRegisterEventHandler(&inp->wnd->text_input_leave,
		inp_handle_leave, inp);
	bRegisterEventHandler(&inp->wnd->text_input_enter,
		inp_handle_enter, inp);
	bRegisterEventHandler(&inp->wnd->keydown,
		inp_handle_keydown, inp);
	bRegisterEventHandler(&inp->wnd->keyup,
		inp_handle_keyup, inp);
	return 1;
}

static int inp_unfocus_handler(void *param, void *data)
{
	bInputWidget *inp = data;
	inp->box->color = inp->default_bg_color;
	bUnregisterEventHandler(&inp->wnd->text_input_update,
		inp_handle_update, inp);
	bUnregisterEventHandler(&inp->wnd->text_input_leave,
		inp_handle_leave, inp);
	bUnregisterEventHandler(&inp->wnd->text_input_enter,
		inp_handle_enter, inp);
	bUnregisterEventHandler(&inp->wnd->keydown,
		inp_handle_keydown, inp);
	bUnregisterEventHandler(&inp->wnd->keyup,
		inp_handle_keyup, inp);
	inp->focused = 0;
	inp_ime_disable(inp);
	return 1;
}

bInputWidget *bCreateInputWidget(const char *text, bWindow *wnd, bInputStyle st,
	bInputMode mode)
{
	bInputWidget *inp;
	assert(st != B_INPUT_STYLE_MULTILINE || ("Not implemented" && 0));
	assert(mode != B_INPUT_MODE_PASSWORD || ("Not implemented" && 0));

	inp = malloc(sizeof(bInputWidget));
	if(!inp) return NULL;

	inp->st = st;
	inp->mode = mode;
	inp->bufsize = 512;
	inp->box = bCreateBoxWidget(B_ALIGN_START, B_BOX_VBOX, -1, 0);
	if(!inp->box){
		free(inp);
		return NULL;
	}
	inp->box->focusable = 1;

	inp->outerbox = bCreateBoxWidget(B_ALIGN_START, B_BOX_VBOX, -1, 0);
	if(!inp->outerbox){
		bDestroyBoxRecursive(inp->box);
		free(inp);
		return NULL;
	}
	bAddBoxToBox(inp->box, inp->outerbox);
	inp->innerbox = bCreateBoxWidget(B_ALIGN_START, B_BOX_HBOX, -1, 38);
	if(!inp->innerbox){
		bDestroyBoxRecursive(inp->box);
		free(inp);
		return NULL;
	}
	bAddBoxToBox(inp->outerbox, inp->innerbox);
	inp->textbox = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_HBOX, -1, 0);
	if(!inp->textbox){
		bDestroyBoxRecursive(inp->box);
		free(inp);
		return NULL;
	}
	bAddBoxToBox(inp->innerbox, inp->textbox);
	inp->cont = bCreateContentWidget(B_ALIGN_START, 0);
	if(!inp->cont){
		bDestroyBoxRecursive(inp->box);
		free(inp);
		return NULL;
	}
	bAddContentToBox(inp->textbox, inp->cont);
	bSetTextContent(inp->cont, "", PANGO_WEIGHT_NORMAL, 16);

	inp->default_bg_color = bColorFromTheme("input:background");
	inp->text_bg_color = bColorFromTheme("input:field:background");
	inp->text_fg_color = bColorFromTheme("input:text");
	inp->selected_bg_color = bColorFromTheme("input:selected:background");
	inp->focused = 0;
	inp->active = 0;
	inp->cursor = 0;
	inp->wnd = wnd;
	inp->box->color = inp->default_bg_color;
	inp->outerbox->color = inp->text_fg_color;
	inp->innerbox->color = inp->text_bg_color;
	inp->textbox->color = inp->text_bg_color;
	inp->cont->color = inp->text_fg_color;

	inp->outerbox->mgn_l =
		inp->outerbox->mgn_t =
		inp->outerbox->mgn_r =
		inp->outerbox->mgn_b = 10;
	inp->innerbox->mgn_l =
		inp->innerbox->mgn_t =
		inp->innerbox->mgn_r =
		inp->innerbox->mgn_b = 1;
	inp->innerbox->clip = 1;
	inp->textbox->mgn_l =
		inp->textbox->mgn_r = 5;

	bRegisterEventHandler(&inp->box->focusin, inp_focus_handler, inp);
	bRegisterEventHandler(&inp->box->focusout, inp_unfocus_handler, inp);

	inp->text = malloc(inp->bufsize * sizeof(wchar_t));
	if(!inp->text){
		bDestroyBoxRecursive(inp->box);
		free(inp);
		return NULL;
	}
	inp->text[0] = L'\0';


	return inp;
}

char *bGetInputText(bInputWidget *inp)
{
	return strdup(inp->cont->text);
}
