/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_WINDOW_H_
#define _BANANUI_WINDOW_H_

#include <wayland-client.h>
#include <cairo.h>
#include <bananui/utils.h>
#include <bananui/keys.h>
#include <bananui/window_title.h>

struct registry_interfaces {
	struct zwp_text_input_manager_v3 *text_input;
	struct wl_compositor *comp;
	struct wl_seat *seat;
	struct wl_shm *shm;
	struct xdg_wm_base *wm_base;
};

struct text_input_state {
	char *preedit, *commit_string;
	uint32_t delete_before, delete_after;
};

typedef struct bsWindow {
	/* public */
	bEventHandler *keydown, *keyup, *fullscreenchange,
		*focusin, *focusout,
		*redraw;
	/* public, readonly */
	cairo_surface_t *surf;
	cairo_t *cr;
	int width, height, closing;
	/* may be used by others, but not really public */
	struct zwp_text_input_v3 *text_input;
	struct wl_surface *wlsurf;
	struct wl_display *disp;
	struct wl_buffer *buffer;
	struct wl_shm_pool *pool;
	unsigned char *pixels;
	int shmfd;
	struct xdg_toplevel *toplevel;
	struct xdg_surface *xdgsurf;
	struct wl_keyboard *keyboard;
	struct xkb_keymap *keymap;
	struct xkb_context *xkbctx;
	struct xkb_state *xkbstate;
	struct text_input_state tis;
	struct registry_interfaces reg;
	int configured_surface, configured_toplevel;
	bEventHandler *text_input_update, *text_input_leave, *text_input_enter;
} bWindow;

bWindow *bCreateWindow(const char *title);
bWindow *bCreateFullscreenWindow(const char *title);
bWindow *bCreateSizedWindow(const char *title, int width, int height);
void bRedrawWindow(bWindow *wnd);
void bDestroyWindow(bWindow *wnd);
int bGetWindowFd(bWindow *wnd);
void bHandleWindowEvent(bWindow *wnd);

#endif
