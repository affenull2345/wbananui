/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_VIEW_H_
#define _BANANUI_VIEW_H_

#include <bananui/utils.h>
#include <bananui/widget.h>
#include <bananui/window.h>
#include <bananui/softkey.h>

typedef struct bsView {
	/* Public */
	bEventHandler *keyup, *keydown;
	bBoxWidget *body, *bg;
	bBoxWidget *header;
	bSoftkeyPanel *sk;
	/* readonly */ unsigned int scrollable;

	/* Private */
	double sy, sx, height, header_height;
	unsigned int rendered;
	bWindow *cur_wnd;
	bWidgetListItem *selected;
} bView;

void bShowView(bWindow *wnd, bView *view);
void bHideView(bWindow *wnd, bView *view);
bView *bCreateView(int scrollable);
void bDestroyView(bView *view);

#endif
