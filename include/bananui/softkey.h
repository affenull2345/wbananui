/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_SOFTKEY_H_
#define _BANANUI_SOFTKEY_H_
#include <cairo.h>
#include <bananui/widget.h>

#define SOFTKEY_HEIGHT 30

typedef struct bsSoftkeyPanel {
	bBoxWidget *main, *lbox, *cbox, *rbox;
	bContentWidget *lcont, *ccont, *rcont;
	int scrheight;
} bSoftkeyPanel;

bSoftkeyPanel *bCreateSoftkeyPanel(void);
void bDestroySoftkeyPanel(bSoftkeyPanel *sk);
void bSetSoftkeyColor(bSoftkeyPanel *sk, bWidgetColor fg, bWidgetColor bg);
void bSetSoftkeyText(bSoftkeyPanel *sk,
	const char *l, const char *c, const char *r);
int bShowSoftkeyPanel(cairo_surface_t *surf, cairo_t *cr, bSoftkeyPanel *sk);

#endif
