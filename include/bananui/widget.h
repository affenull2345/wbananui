/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_WIDGET_H_
#define _BANANUI_WIDGET_H_

#include <pango/pangocairo.h>
#include <bananui/utils.h>

/*=================== Enums ===================*/
typedef enum beContentAlign {
	B_ALIGN_START,
	B_ALIGN_CENTER,
	B_ALIGN_END
} bContentAlign;

typedef enum beContentFlags {
	B_CONTENT_END_AT_LAST_BASELINE = 1 /* Ignored */
} bContentFlags;

typedef enum beContentWrapMode {
	B_WRAP_ELLIPSIZE,
	B_WRAP_BREAK,
	B_WRAP_WORD
} bContentWrapMode;

typedef enum beBoxDirection {
	B_BOX_VBOX,
	B_BOX_HBOX
} bBoxDirection;

typedef enum beContentType {
	B_CONTENT_TYPE_EMPTY,
	B_CONTENT_TYPE_TEXT,
	B_CONTENT_TYPE_IMAGE,
	B_CONTENT_TYPE_IMAGE_SVG
} bContentType;

typedef enum beColorType {
	B_COLOR_TYPE_SIMPLE
} bColorType;

typedef enum beWidgetType {
	B_WIDGET_TYPE_BOX,
	B_WIDGET_TYPE_CONTENT
} bWidgetType;

/*==================== Structs ========================*\
| Fields not declared as public are considered private. |
\*=====================================================*/

typedef struct bsRsvgHandleWrapper bRsvgHandleWrapper;

typedef struct bsWidgetListItem {
	struct bsBoxWidget *owner;
	bWidgetType type;
	union {
		struct bsBoxWidget *box;
		struct bsContentWidget *cont;
	};
	struct bsWidgetListItem *prev, *next;
} bWidgetListItem;

typedef struct bsWidgetList {
	bWidgetListItem *first, *last;
} bWidgetList;

typedef struct bsColorSimple {
	double r, g, b, a;
} bColorSimple;

typedef struct bsWidgetColor {
	bColorType type;
	bColorSimple simple;
} bWidgetColor;

typedef struct bsComputedLayout {
	/* WARNING: Public values are undefined before render/compute */
	/* public */ double width, height;
	/* public */ double top, left;

	double used_space;
	int num_stretches;
} bComputedLayout;

typedef struct bsBoxWidget {
	/* readonly */ bBoxDirection dir;
	bWidgetList children;
	/* public */ bWidgetColor color;
	/* readonly */ bContentAlign align;
	/* public */ bComputedLayout computed;

	/* <0 means stretch, =0 means use minimum needed space */
	/* readonly */ double width, height;

	/* public */ double pad_l, pad_t, pad_r, pad_b;
	/* public */ double mgn_l, mgn_t, mgn_r, mgn_b;

	/* public */ unsigned int focusable, clip;

	/* public */ bEventHandler *focusin, *focusout, *click, *skright,
		*skleft;
	struct bsWidgetListItem *self;
} bBoxWidget;

typedef struct bsContentWidget {
	double imgscale, svgwidth, svgheight, offset;
	int cursor, cursor_visible;
	int svgrecolor;
	PangoFontDescription *font;
	PangoLayout *lt;
	/* readonly */ bContentAlign align;
	/* readonly */ bContentFlags flags;
	/* readonly */ bContentType type;
	bContentWrapMode wrapmode;
	union {
		char *text;
		cairo_surface_t *imgsurf;
		bRsvgHandleWrapper *rsvghandle;
		void *data;
	};
	/* public */ bWidgetColor color;
	/* public */ bComputedLayout computed;
	struct bsWidgetListItem *self;
} bContentWidget;

/* Public API */

int bComputeLayout(cairo_surface_t *surf, cairo_t *cr, bBoxWidget *box);
int bShowMainBox(cairo_surface_t *surf, cairo_t *cr, bBoxWidget *box,
	double top, double left);

int bAddBoxToBox(bBoxWidget *box, bBoxWidget *child);
int bAddContentToBox(bBoxWidget *box, bContentWidget *cont);
int bAddFillToBox(bBoxWidget *box);

/* width/height <0 means stretch, =0 means use minimum needed space */
bBoxWidget *bCreateBoxWidget(bContentAlign align,
	bBoxDirection dir, double width, double height);
bContentWidget *bCreateContentWidget(bContentAlign align, bContentFlags flags);

int bSetTextContent(bContentWidget *cont, const char *txt, PangoWeight weight,
	double size);
int bLoadImageFromFile(bContentWidget *cont, const char *filename);
int bLoadImageFromIcon(bContentWidget *cont, const char *name, int size);

bWidgetColor bColorFromTheme(const char *name);

static inline bWidgetColor
bRGBA(double r, double g, double b, double a)
{
	bWidgetColor clr;
	clr.type = B_COLOR_TYPE_SIMPLE;
	clr.simple.r = r;
	clr.simple.g = g;
	clr.simple.b = b;
	clr.simple.a = a;
	return clr;
}

#define B_RGBA_INITIALIZER(r, g, b, a) { \
	.type = B_COLOR_TYPE_SIMPLE, \
	.simple = { r, g, b, a } \
}

static inline __attribute__((deprecated("Use bRGBA instead"))) void
bSetRGBA(bWidgetColor *clr, double r, double g, double b, double a)
{
	clr->type = B_COLOR_TYPE_SIMPLE;
	clr->simple.r = r;
	clr->simple.g = g;
	clr->simple.b = b;
	clr->simple.a = a;
}

void bDestroyBoxRecursive(bBoxWidget *box);
void bDestroyBoxChildren(bBoxWidget *box);

extern const char *bGlobalAppClass;

#endif
