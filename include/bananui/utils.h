/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_UTILS_H_
#define _BANANUI_UTILS_H_

#include <cairo.h>

#define auto_surface_get_height(_surf) \
		cairo_image_surface_get_height(_surf)
#define auto_surface_get_width(_surf) \
		cairo_image_surface_get_width(_surf)

typedef int (*bEventCallback)(void *param, void *userdata);

typedef struct bsEventHandler {
	bEventCallback cb;
	void *userdata;
	struct bsEventHandler *next;
} bEventHandler;

void bRegisterEventHandler(bEventHandler **list,
		bEventCallback cb, void *userdata);
void bUnregisterEventHandler(bEventHandler **list,
		bEventCallback cb, void *userdata);
int bTriggerEvent(bEventHandler *list, void *param);


#endif
