/* vim: set sw=2 sts=2 et: */

static struct sbItem radioUI(bContentWidget **w_pri, bContentWidget **w_sec,
	bContentWidget **w_freq)
{
  struct sbItem pri, sec, freq, ret = sbBOX(B_ALIGN_START, B_BOX_VBOX, -1, -1,
    sbBOX(B_ALIGN_CENTER, B_BOX_HBOX, -1, 0,
      sbBOX(B_ALIGN_START, B_BOX_VBOX, -1, -1, sbEND),
      sbBOX(B_ALIGN_CENTER, B_BOX_VBOX, -1, -1,
        sbTEXT(B_ALIGN_CENTER, "--.-", PANGO_WEIGHT_NORMAL, 30,
          sbREF(&freq), sbEND),
        sbEND
      ),
      sbBOX(B_ALIGN_START, B_BOX_VBOX, -1, -1, sbEND),
      sbEND
    ),
    sbTEXT(B_ALIGN_START, START_TEXT, PANGO_WEIGHT_BOLD, 18,
      sbREF(&pri), sbEND),
    sbTEXT(B_ALIGN_START, "", PANGO_WEIGHT_NORMAL, 16,
      sbREF(&sec), sbEND),
    sbEND
  );
  if(pri.type == SB_ITEM_CONTENT) *w_pri = pri.cont;
  if(sec.type == SB_ITEM_CONTENT) *w_sec = sec.cont;
  if(freq.type == SB_ITEM_CONTENT) *w_freq = freq.cont;
  return ret;
}
