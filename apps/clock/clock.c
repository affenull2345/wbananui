#include <bananui/window.h>
#include <bananui/widget.h>
#include <bananui/view.h>
#include "stopwatch.h"

bWidgetColor color_text, color_back, color_transp;

static int handle_keydown(void *param, void *data)
{
	xkb_keysym_t *sym = param;

	if(*sym == XKB_KEY_BackSpace || *sym == XKB_KEY_Escape)
		exit(0);

	return 1;
}

int main(void)
{
	bView *v_stopwatch;
	bWindow *wnd;

	bGlobalAppClass = "utility";

	color_text = bColorFromTheme("text");
	color_back = bColorFromTheme("background");
	color_transp = bRGBA(0.0, 0.0, 0.0, 0.0);

	wnd = bCreateWindow("About");
	if(!wnd) return 1;

	bRegisterEventHandler(&wnd->keydown, handle_keydown, NULL);

	v_stopwatch = create_stopwatch_view(wnd);

	bShowView(wnd, v_stopwatch);

	bRedrawWindow(wnd);

	while(!wnd->closing){
		int res, fd;
		fd_set fds;

		fd = bGetWindowFd(wnd);

		FD_ZERO(&fds);
		FD_SET(fd, &fds);

		res = select(fd+1, &fds, NULL, NULL, NULL);

		if(res < 0 && errno != EINTR){
			perror("select");
			return 1;
		}
		if(res > 0){
			if(FD_ISSET(fd, &fds)){
				bHandleWindowEvent(wnd);
			}
		}
	}

	return 0;
}
