#include <bananui/window.h>
#include <bananui/widget.h>
#include <bananui/view.h>
#include <bananui/highlevel.h>

#define NUM_LICENSES 2

static const char header_text[] = "Bananui user interface";

static const char about_text[] =
#ifdef BANANIAN_VERSION
"Built for Bananian v" BANANIAN_VERSION "\n"
#endif
"\n"
"Bananui is a user interface for smart feature phones.\n"
"wbananui is a complete re-write of the old UI, and uses Wayland for "
"displaying/compositing windows and Cairo for rendering.\n\n"
"wbananui is Copyright (C) 2021 Affe Null <affenull2345@gmail.com>.\n"
"wbananui is free software: you can redistribute it and/or modify it under "
"the terms of the GNU Lesser General Public License as published by the "
"Free Software Foundation, either version 3 of the License, or any later "
"version.\n\n"
"The user interface is distributed in the hope that it will be useful, but "
"WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY "
"or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License"
" for more details.";

static bWidgetColor color_text, color_back;

struct application_state {
	bView *v_about, *v_licenses, *v_license[NUM_LICENSES];
	bWindow *wnd;
	int running;
	enum {
		ACTIVE_ABOUT,
		ACTIVE_LICENSES,
		ACTIVE_LICENSE
	} active;
};

static int handle_keydown(void *param, void *data)
{
	struct application_state *as = data;
	xkb_keysym_t *sym = param;

	if(*sym == XKB_KEY_Escape){
		as->running = 0;
		return 1;
	}

	if(as->active == ACTIVE_ABOUT){
		if(*sym == XKB_KEY_Return || *sym == XKB_KEY_BackSpace)
			as->running = 0;
		else if(*sym == BANANUI_KEY_SoftRight){
			bHideView(as->wnd, as->v_about);
			as->active = ACTIVE_LICENSES;
			bShowView(as->wnd, as->v_licenses);
			bRedrawWindow(as->wnd);
		}
	}
	else if(as->active == ACTIVE_LICENSES){
		if(*sym == XKB_KEY_BackSpace){
			bHideView(as->wnd, as->v_licenses);
			as->active = ACTIVE_ABOUT;
			bShowView(as->wnd, as->v_about);
			bRedrawWindow(as->wnd);
		}
	}

	return 1;
}

int main(void)
{
	bContentWidget *about, *header;
	bButtonWidget *btn_gpl, *btn_lgpl;
	struct application_state as;

	color_text = bColorFromTheme("text");
	color_back = bColorFromTheme("background");

	as.wnd = bCreateWindow("About");
	if(!as.wnd) return 1;

	bRegisterEventHandler(&as.wnd->keydown, handle_keydown, &as);

	as.v_about = bCreateView(1);
	as.v_about->sk = bCreateSoftkeyPanel();

	as.v_about->body->mgn_l = as.v_about->body->mgn_r = 2;

	bSetSoftkeyText(as.v_about->sk, "", "CLOSE", "Licenses");

	header = bCreateContentWidget(B_ALIGN_CENTER, 0);
	about = bCreateContentWidget(B_ALIGN_START, 0);
	bSetTextContent(header, header_text, PANGO_WEIGHT_BOLD, 20);
	bSetTextContent(about, about_text, PANGO_WEIGHT_NORMAL, 17);
	bAddContentToBox(as.v_about->body, header);
	bAddContentToBox(as.v_about->body, about);

	as.v_about->bg->color = color_back;
	header->color = color_text;
	about->color = color_text;

	as.v_licenses = bCreateView(1);
	as.v_licenses->sk = bCreateSoftkeyPanel();

	bSetSoftkeyText(as.v_licenses->sk, "", "SELECT", "");

	btn_gpl = bCreateButtonWidget("GNU GPL v3.0", B_BUTTON_STYLE_MENU,
		B_ALIGN_START, -1, 60);
	btn_lgpl = bCreateButtonWidget("GNU LGPL v3.0", B_BUTTON_STYLE_MENU,
		B_ALIGN_START, -1, 60);

	bAddBoxToBox(as.v_licenses->body, btn_gpl->box);
	bAddBoxToBox(as.v_licenses->body, btn_lgpl->box);

	as.v_licenses->bg->color = color_back;

	bShowView(as.wnd, as.v_about);

	as.active = ACTIVE_ABOUT;
	as.running = 1;

	bRedrawWindow(as.wnd);

	while(as.running && !as.wnd->closing){
		int res, fd;
		fd_set fds;

		fd = bGetWindowFd(as.wnd);

		FD_ZERO(&fds);
		FD_SET(fd, &fds);

		res = select(fd+1, &fds, NULL, NULL, NULL);

		if(res < 0 && errno != EINTR){
			perror("select");
			return 1;
		}
		if(res > 0){
			if(FD_ISSET(fd, &fds)){
				bHandleWindowEvent(as.wnd);
			}
		}
	}

	return 0;
}
