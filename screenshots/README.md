Home screen:

![Home](Home.png)

Window list:

![WindowList](WindowList.png)

App list:

![AppList](AppList.png)

Navigation pop-up:

![Nav](Nav.png)

About text:

![AboutText](AboutText.png)

About menu:

![AboutMenu](AboutMenu.png)

Test app:

![Test1](Test1.png)
![Test2](Test2.png)
![Test3](Test3.png)
![Test4](Test4.png)
![Test5](Test5.png)
