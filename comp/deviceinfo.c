/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <linux/input-event-codes.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <systemd/sd-bus.h>
#include "deviceinfo.h"

struct device_info {
	char *power_button_name, *lid_device_name;
	sd_bus *bus;
};

struct device_info *device_info_load(void)
{
	struct device_info *di;
	di = calloc(1, sizeof(struct device_info));
	if(!di) return NULL;
	di->power_button_name = strdup("qpnp_pon");
	di->lid_device_name = strdup("hall_sensor0");
	sd_bus_default_system(&di->bus);
	return di;
}

int device_should_wakeup(struct device_info *di, const char *input_name,
		uint32_t keycode, int released)
{
	if(released){
		if(di->lid_device_name && input_name &&
			0 == strcmp(input_name, di->lid_device_name) &&
			keycode == DEV_KEY_SWITCH)
		{
			return 1;
		}
	}
	else {
		if(di->power_button_name && input_name &&
			0 == strcmp(input_name, di->power_button_name))
		{
			return 1;
		}
	}
	return 0;
}
int device_should_sleep(struct device_info *di, const char *input_name,
		uint32_t keycode, int released)
{
	if(released){
		if(di->power_button_name && input_name && keycode == KEY_POWER
			&& 0 == strcmp(input_name, di->power_button_name))
		{
			return 1;
		}
	}
	else {
		if(di->lid_device_name && input_name &&
			0 == strcmp(input_name, di->lid_device_name) &&
			keycode == DEV_KEY_SWITCH)
		{
			return 1;
		}
	}
	return 0;
}


uint32_t device_patch_keycode(struct device_info *di, const char *input_name,
		uint32_t orig)
{
	if(!di) return orig;
	switch(orig){
		/* Center key */
		case KEY_OK:
			return KEY_ENTER;
		/* '*' key */
		case KEY_NUMERIC_STAR:
			return KEY_LEFTCTRL;
		/* '#' key */
		case KEY_NUMERIC_POUND:
			return KEY_LEFTALT;
		/* Left softkey */
		case KEY_MENU:
			return KEY_TAB;
		/* Right softkey */
		case KEY_BACK:
			return KEY_COMPOSE;
		/* End call key acts as backspace on many devices */
		case KEY_POWER:
			if(!di->power_button_name || !input_name ||
				0 != strcmp(input_name, di->power_button_name))
			{
				return KEY_BACKSPACE;
			}
			break;
		/* Call key */
		case KEY_SEND:
			return KEY_F3;
		default:
			break;
	}
	return orig;
}

void device_suspend(struct device_info *di)
{
	int ret;
	sd_bus_message *msg = NULL;
	sd_bus_error error = SD_BUS_ERROR_NULL;

	/*
	ret = sd_bus_call_method(di->bus, "org.freedesktop.login1",
		"/org/freedesktop/login1", "org.freedesktop.login1.Manager",
		"Suspend", &error, &msg, "b", 0);
	if(ret < 0){
		fprintf(stderr, "device: Suspend via systemd failed: %s\n",
			error.message);
	}
	*/

	ret = sd_bus_call_method(di->bus, "org.freedesktop.login1",
		"/org/freedesktop/login1/session/auto",
		"org.freedesktop.login1.Session", "SetIdleHint",
		&error, &msg, "b", 1);
	if(ret < 0){
		fprintf(stderr, "device: Setting session idle failed: %s\n",
			error.message);
	}

	sd_bus_error_free(&error);
	sd_bus_message_unref(msg);
}

void device_resume(struct device_info *di)
{
	int ret;
	sd_bus_message *msg = NULL;
	sd_bus_error error = SD_BUS_ERROR_NULL;

	ret = sd_bus_call_method(di->bus, "org.freedesktop.login1",
		"/org/freedesktop/login1/session/auto",
		"org.freedesktop.login1.Session", "SetIdleHint",
		&error, &msg, "b", 0);
	if(ret < 0){
		fprintf(stderr, "device: Setting session idle failed: %s\n",
			error.message);
	}

	sd_bus_error_free(&error);
	sd_bus_message_unref(msg);
}

static int wakelocked(void)
{
	FILE *locks = fopen("/sys/power/wake_lock", "r");
	int ch;
	if(!locks) return 0;
	do {
		ch = fgetc(locks);
	} while(ch == '\n');
	fclose(locks);
	return ch != EOF;
}

void device_ensure_awake(struct device_info *di)
{
	int valid = 0;
	char buf[3];
	while(1){
		FILE *sleep = fopen("/sys/power/autosleep", "r");
		if(!sleep) return;
		valid = (3 == fread(buf, 1, 3, sleep));
		fclose(sleep);
		if((!valid || 0 != strncmp(buf, "off", 3)) && !wakelocked())
			usleep(100000);
		else
			break;
	}
}

void device_info_free(struct device_info *di)
{
	if(!di) return;
	if(di->power_button_name) free(di->power_button_name);
	free(di);
}
