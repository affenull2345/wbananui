/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _COMP_SERVER_H_
#define _COMP_SERVER_H_

#include <wayland-server.h>

struct server_data {
	struct wl_display *disp;
	struct wl_event_loop *loop;

	struct wlr_backend *backend;
	struct wlr_compositor *comp;
	struct wlr_seat *seat;
	struct wlr_xdg_shell *shell;

	struct wlr_text_input_manager_v3 *txtmgr;
	struct wlr_screencopy_manager_v1 *scrcpy;
	double cur_x, cur_y;
	int fullscreen_height, fullscreen_width;
	int viewport_height, viewport_width;

	bool sleeping;

	struct input_data *inp;

	struct text_input_info *txtinp;
	struct device_info *di;
	struct topbar_data *topbar;
	struct homescreen_data *homescreen;
	struct navigation_data *nav;

	struct wlr_output_layout *layout;

	struct window_manager *wm;
	/*
	struct toplevel_item *active;
	*/

	struct wl_listener new_output;
	struct wl_listener new_input;
	struct wl_listener new_surface;
	struct wl_listener new_xdg_surface;

	struct wl_event_source *clocktimer;

	struct wl_list outputs;
	struct wl_list inputs;
	struct wl_list surfaces;
	struct wl_list toplevels;
};

void ui_sleep(struct server_data *srv);
void ui_wakeup(struct server_data *srv);

#endif
