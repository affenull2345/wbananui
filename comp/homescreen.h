/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _COMP_HOMESCREEN_H_
#define _COMP_HOMESCREEN_H_

#include <wlr/render/wlr_texture.h>
#include <wlr/render/wlr_renderer.h>
#include <xkbcommon/xkbcommon.h>

struct homescreen_data;
enum homescreen_action {
	HS_IGNORE,
	HS_RENDER,
	HS_LAUNCHING
};

struct homescreen_data *init_homescreen(unsigned int width,
	unsigned int height, struct wlr_renderer *renderer);
struct wlr_texture *homescreen_get_texture(struct homescreen_data *hs);
struct wlr_texture *homescreen_get_applist_texture(struct homescreen_data *hs);
/*void homescreen_repaint_into_texture(struct homescreen_data *hs);*/
void homescreen_set_clock(struct homescreen_data *hs, const char *str);

enum homescreen_action homescreen_handle_key(struct homescreen_data *hs,
	xkb_keysym_t key);
/* Return value: true if needs rendering */
bool homescreen_damage(struct homescreen_data *hs);
double homescreen_next_alpha(struct homescreen_data *hs, long time);

#endif
