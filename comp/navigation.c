/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <wlr/render/wlr_renderer.h>
#include <wlr/render/wlr_texture.h>
#include <wlr/types/wlr_output.h>
#include <bananui/widget.h>
#include "server.h"
#include "topbar.h"
#include "output.h"
#include "wm.h"
#include "screenshot.h"
#include "navigation.h"

#define WINLIST_WINDOW_DISTANCE 0.85
#define NAV_SIZE 154

void winlist_close(struct server_data *srv, bool animate)
{
	if(animate){
		srv->nav->wl_dpos = -1;
		srv->nav->wl_openclose = true;
	}
	else 
		srv->nav->wl_pos = 0;
}

static void winlist_focus_selected_window(struct server_data *srv)
{
	winlist_close(srv, false);
	toplevel_activate(srv,
			window_find_by_index(srv, srv->nav->wl_selected));
}
static void winlist_close_selected_window(struct server_data *srv)
{
	struct toplevel_item *toplevel =
		window_find_by_index(srv, srv->nav->wl_selected);
	if(toplevel) window_close(toplevel);
}
static void winlist_select_active(struct server_data *srv)
{
	int i = 0;
	if(srv->wm->active){
		i = window_get_index(srv->wm->active);
	}
	if(i < 0) i = 0;
	srv->nav->wl_selected = i;
	srv->nav->wl_xoffset = -(i * srv->fullscreen_width *
				WINLIST_WINDOW_DISTANCE);
}

void winlist_handle_key(struct server_data *srv, xkb_keysym_t sym,
	uint32_t modifiers)
{
	if(sym == XKB_KEY_BackSpace)
	{
		winlist_close(srv, true);
		damage_app_output(srv);
	}
	else if(sym == XKB_KEY_Right && srv->nav->wl_dxoffset == 0)
	{
		if(srv->nav->wl_selected < srv->nav->wl_max-1){
			srv->nav->wl_selected++;
			srv->nav->wl_move = true;
			srv->nav->wl_dxoffset =
				-(srv->fullscreen_width *
				WINLIST_WINDOW_DISTANCE);
			srv->nav->wl_destxoffset =
				srv->nav->wl_xoffset +
				srv->nav->wl_dxoffset;
			damage_app_output(srv);
		}
		else if(srv->nav->wl_max > 0){
			srv->nav->wl_move = true;
			srv->nav->wl_dxoffset =
				srv->fullscreen_width *
				WINLIST_WINDOW_DISTANCE *
				srv->nav->wl_selected;
			srv->nav->wl_selected = 0;
			srv->nav->wl_destxoffset = 0;
			damage_app_output(srv);
		}
	}
	else if(sym == XKB_KEY_Left && srv->nav->wl_dxoffset == 0)
	{
		if(srv->nav->wl_selected > 0){
			srv->nav->wl_selected--;
			srv->nav->wl_move = true;
			srv->nav->wl_dxoffset =
				srv->fullscreen_width *
				WINLIST_WINDOW_DISTANCE;
			srv->nav->wl_destxoffset =
				srv->nav->wl_xoffset +
				srv->nav->wl_dxoffset;
			damage_app_output(srv);
		}
		else if(srv->nav->wl_max > 0){
			srv->nav->wl_selected =
				srv->nav->wl_max - 1;
			srv->nav->wl_move = true;
			srv->nav->wl_dxoffset =
				-(srv->fullscreen_width *
				WINLIST_WINDOW_DISTANCE *
				srv->nav->wl_selected);
			srv->nav->wl_destxoffset =
				srv->nav->wl_dxoffset;
			damage_app_output(srv);
		}
	}
	else if(sym == XKB_KEY_Up)
	{
		winlist_close_selected_window(srv);
		damage_app_output(srv);
	}
	else if(sym == XKB_KEY_Down)
	{
		winlist_close(srv, true);
		toplevel_activate(srv, NULL);
	}
	else if(sym == XKB_KEY_Return)
	{
		winlist_focus_selected_window(srv);
	}
}

void nav_handle_key(struct server_data *srv, xkb_keysym_t sym,
	uint32_t modifiers, int released)
{
	if(released){
		if(sym == XKB_KEY_BackSpace){
			srv->nav->nav_open = false;
			damage_app_output(srv);
		}
	}
	else {
		if(sym == XKB_KEY_Right){
			srv->nav->nav_open = false;
			srv->nav->wl_pos = 0.0;
			srv->nav->wl_dpos = 1.0;
			srv->nav->wl_openclose = true;
			srv->nav->wl_move = false;
			srv->nav->wl_xoffset = 0.0;
			srv->nav->wl_dxoffset = 0.0;
			srv->nav->wl_destxoffset = 0.0;
			srv->nav->wl_selected = 0;
			srv->nav->wl_max = 0;
			winlist_select_active(srv);
			damage_app_output(srv);
		}
		else if(sym == XKB_KEY_Down){
			srv->nav->nav_open = false;
			toplevel_activate(srv, NULL);
		}
		else if(sym == XKB_KEY_Return){
			screenshot();
			srv->nav->nav_open = false;
			damage_app_output(srv);
		}
	}
}

static struct wlr_texture *create_nav_texture(struct wlr_renderer *renderer)
{
	unsigned int stride;
	unsigned char *pixels;
	cairo_surface_t *csurf;
	cairo_t *cr;
	bBoxWidget *mainbox, *box2;
	bBoxWidget *ic1, *ic2, *ic3, *ic4, *ic5;
	bContentWidget *i1, *i2, *i3, *i4, *i5;

	stride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32, NAV_SIZE);
	pixels = calloc(1, stride*NAV_SIZE);
	if(!pixels) return NULL;
	csurf = cairo_image_surface_create_for_data(pixels,
		CAIRO_FORMAT_ARGB32, NAV_SIZE, NAV_SIZE, stride);
	if(!csurf){
		free(pixels);
		return NULL;
	}
	cr = cairo_create(csurf);
	if(!cr){
		free(pixels);
		cairo_surface_destroy(csurf);
		return NULL;
	}

	mainbox = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_VBOX,
				NAV_SIZE-10, NAV_SIZE-10);
	box2 = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_HBOX,
				NAV_SIZE-10, 48);
	ic1 = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_HBOX, 32, 32);
	ic1->mgn_l = ic1->mgn_t = ic1->mgn_b = ic1->mgn_r = 8;
	ic2 = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_HBOX, 32, 32);
	ic2->mgn_l = ic2->mgn_t = ic2->mgn_b = ic2->mgn_r = 8;
	ic3 = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_HBOX, 32, 32);
	ic3->mgn_l = ic3->mgn_t = ic3->mgn_b = ic3->mgn_r = 8;
	ic4 = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_HBOX, 32, 32);
	ic4->mgn_l = ic4->mgn_t = ic4->mgn_b = ic4->mgn_r = 8;
	ic5 = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_HBOX, 32, 32);
	ic5->mgn_l = ic5->mgn_t = ic5->mgn_b = ic5->mgn_r = 8;
	i1 = bCreateContentWidget(B_ALIGN_CENTER, 0);
	i2 = bCreateContentWidget(B_ALIGN_CENTER, 0);
	i3 = bCreateContentWidget(B_ALIGN_CENTER, 0);
	i4 = bCreateContentWidget(B_ALIGN_CENTER, 0);
	i5 = bCreateContentWidget(B_ALIGN_CENTER, 0);

	bLoadImageFromIcon(i1, "preferences-system-symbolic", 32);
	bLoadImageFromIcon(i2, /* Not really preferences, just notifications */
		"preferences-system-notifications-symbolic", 32);
	bLoadImageFromIcon(i3, "applets-screenshooter-symbolic", 32);
	bLoadImageFromIcon(i4, "view-list-symbolic", 32);
	bLoadImageFromIcon(i5, "go-home-symbolic", 32);

	i1->color = bRGBA(1.0, 1.0, 1.0, 1.0);
	i1->color = bRGBA(1.0, 1.0, 1.0, 1.0);
	i2->color = bRGBA(1.0, 1.0, 1.0, 1.0);
	i3->color = bRGBA(1.0, 1.0, 1.0, 1.0);
	i4->color = bRGBA(1.0, 1.0, 1.0, 1.0);
	i5->color = bRGBA(1.0, 1.0, 1.0, 1.0);

	bAddContentToBox(ic1, i1);
	bAddContentToBox(ic2, i2);
	bAddContentToBox(ic3, i3);
	bAddContentToBox(ic4, i4);
	bAddContentToBox(ic5, i5);
	bAddBoxToBox(mainbox, ic1);
	bAddBoxToBox(box2, ic2);
	bAddBoxToBox(box2, ic3);
	bAddBoxToBox(box2, ic4);
	bAddBoxToBox(mainbox, box2);
	bAddBoxToBox(mainbox, ic5);

	cairo_arc(cr, NAV_SIZE/2, NAV_SIZE/2, NAV_SIZE/2, 0, 360);
	cairo_set_source_rgba(cr, 0.3, 0.3, 0.3, 0.8);
	cairo_fill(cr);
	bShowMainBox(csurf, cr, mainbox, 5, 5);

	bDestroyBoxRecursive(mainbox);

	cairo_destroy(cr);
	cairo_surface_destroy(csurf);
	return wlr_texture_from_pixels(renderer, WL_SHM_FORMAT_ARGB8888,
			stride, NAV_SIZE, NAV_SIZE, pixels);
}
static struct wlr_texture *create_no_window_texture(
	struct wlr_renderer *renderer, unsigned int width, unsigned int height)
{
	unsigned int stride;
	unsigned char *pixels;
	cairo_surface_t *csurf;
	cairo_t *cr;
	bBoxWidget *mainbox;
	bContentWidget *text;

	stride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32, width);
	pixels = calloc(1, stride*height);
	if(!pixels) return NULL;
	csurf = cairo_image_surface_create_for_data(pixels,
		CAIRO_FORMAT_ARGB32, width, height, stride);
	if(!csurf){
		free(pixels);
		return NULL;
	}
	cr = cairo_create(csurf);
	if(!cr){
		free(pixels);
		cairo_surface_destroy(csurf);
		return NULL;
	}

	mainbox = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_HBOX,
				width, height);
	text = bCreateContentWidget(B_ALIGN_CENTER, 0);
	bSetTextContent(text, "No windows are open.",
			PANGO_WEIGHT_SEMIBOLD, 17);

	text->color = bRGBA(1.0, 1.0, 1.0, 1.0);
	bAddContentToBox(mainbox, text);

	bShowMainBox(csurf, cr, mainbox, 0, 0);

	bDestroyBoxRecursive(mainbox);

	cairo_destroy(cr);
	cairo_surface_destroy(csurf);
	return wlr_texture_from_pixels(renderer, WL_SHM_FORMAT_ARGB8888,
			stride, width, height, pixels);
}

void navigation_damage(struct server_data *srv)
{
	if(srv->nav->wl_dpos != 0 || srv->nav->wl_dxoffset != 0 ||
		srv->nav->damage)
	{
		srv->nav->damage = false;
		damage_app_output(srv);
	}
}

void navigation_surfaces_damaged(struct server_data *srv)
{
	if(srv->nav->wl_pos > 0){
		srv->nav->damage = true;
	}
}

void render_navigation(struct server_data *srv, struct wlr_renderer *renderer,
		struct timespec now, struct wlr_output *out, long time)
{
	if(srv->nav->wl_pos > 0){
		struct wlr_box wlbox;
		float wlbgcolor[] = {0.3f, 0.3f, 0.3f, 0.9f};
		wlbox.y = srv->fullscreen_height - srv->nav->wl_pos;
		wlbox.x = 0;
		wlbox.width = srv->fullscreen_width;
		wlbox.height = srv->fullscreen_height - TOPBAR_HEIGHT;
		wlr_render_rect(renderer, &wlbox, wlbgcolor,
			out->transform_matrix);
		srv->nav->wl_max = window_render_side_by_side(srv,
				renderer, now, out,
				srv->fullscreen_width *
				WINLIST_WINDOW_DISTANCE,
				(srv->fullscreen_width * 0.1) +
				srv->nav->wl_xoffset,
				(srv->fullscreen_height * 1.1) -
				srv->nav->wl_pos - TOPBAR_HEIGHT);
		if(!srv->nav->wl_max){
			wlr_render_texture(renderer,
				srv->nav->no_window_texture,
				out->transform_matrix, 0,
				srv->fullscreen_height -
				srv->nav->wl_pos - TOPBAR_HEIGHT, 1.0f);
		}
		if(srv->nav->wl_max &&
			srv->nav->wl_selected >= srv->nav->wl_max)
		{
			srv->nav->wl_destxoffset = srv->nav->wl_xoffset;
		}
		while(srv->nav->wl_max &&
			srv->nav->wl_selected >= srv->nav->wl_max)
		{
			srv->nav->wl_selected--;
			srv->nav->wl_move = true;
			srv->nav->wl_dxoffset =
				srv->fullscreen_width * WINLIST_WINDOW_DISTANCE;
			srv->nav->wl_destxoffset += srv->nav->wl_dxoffset;
		}
	}
	if(srv->nav->wl_openclose){
		srv->nav->wl_openclose = false;
	}
	else if(srv->nav->wl_dpos > 0){
		srv->nav->wl_pos += srv->fullscreen_height *
					time / 250000.0;
		if(srv->nav->wl_pos >= (srv->fullscreen_height - TOPBAR_HEIGHT))
		{
			srv->nav->wl_pos = srv->fullscreen_height -
				TOPBAR_HEIGHT;
			srv->nav->wl_dpos = 0;
			srv->nav->damage = true;
		}
	}
	else if(srv->nav->wl_dpos < 0){
		srv->nav->wl_pos -= srv->fullscreen_height *
					time / 250000.0;
		if(srv->nav->wl_pos <= 0) {
			srv->nav->wl_pos = 0;
			srv->nav->wl_dpos = 0;
			srv->nav->damage = true;
		}
	}
	if(srv->nav->wl_move){
		srv->nav->wl_move = false;
	}
	else if(srv->nav->wl_dxoffset > 0){
		srv->nav->wl_xoffset += srv->nav->wl_dxoffset *
					time / 250000.0;
		if(srv->nav->wl_xoffset >= srv->nav->wl_destxoffset){
			srv->nav->wl_xoffset = srv->nav->wl_destxoffset;
			srv->nav->wl_dxoffset = 0;
			srv->nav->damage = true;
		}
	}
	else if(srv->nav->wl_dxoffset < 0){
		srv->nav->wl_xoffset += srv->nav->wl_dxoffset *
					time / 250000.0;
		if(srv->nav->wl_xoffset <= srv->nav->wl_destxoffset){
			srv->nav->wl_xoffset = srv->nav->wl_destxoffset;
			srv->nav->wl_dxoffset = 0;
			srv->nav->damage = true;
		}
	}

	if(srv->nav->nav_open){
		wlr_render_texture(renderer, srv->nav->nav_texture,
			out->transform_matrix,
			srv->fullscreen_width / 2 - NAV_SIZE / 2,
			srv->fullscreen_height / 2 - NAV_SIZE / 2, 1.0f);
	}
}

struct navigation_data *init_navigation(unsigned int width, unsigned int height,
		struct wlr_renderer *renderer)
{
	struct navigation_data *nav;

	nav = calloc(1, sizeof(struct navigation_data));
	if(!nav) return NULL;

	nav->no_window_texture = create_no_window_texture(renderer, width,
			height - TOPBAR_HEIGHT);
	nav->nav_texture = create_nav_texture(renderer);

	return nav;
}
