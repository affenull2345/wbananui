/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_surface.h>
#include <wlr/types/wlr_matrix.h>
#include "server.h"
#include "output.h"
#include "navigation.h"
#include "homescreen.h"
#include "topbar.h"
#include "wm.h"
#include "render.h"

void render_damage(struct server_data *srv)
{
	if(!srv->wm->active && homescreen_damage(srv->homescreen)){
		damage_app_output(srv);
	}
	navigation_damage(srv);
}

void render_apps(struct server_data *srv, struct wlr_renderer *renderer,
		struct timespec now, struct wlr_output *out, long time)
{
	int width, height;
	float color[] = {0.15f, 0.15f, 0.15f, 1.0f};

	wlr_renderer_clear(renderer, color);

	if(srv->wm->underlay){
		window_render(srv->wm->underlay, renderer, now, out);
	}
	if(srv->wm->active){
		window_render(srv->wm->active, renderer, now, out);
		if(!window_is_topbar_hidden(srv->wm->active)){
			wlr_render_texture(renderer,
				topbar_get_texture(srv->topbar),
				out->transform_matrix, 0, 0, 1.0f);
		}
	}
	else if(srv->wm->hs_launching_countdown){
		float color[] = {0.0f, 0.0f, 0.0f, 1.0f};
		wlr_renderer_clear(renderer, color);
	}
	else {
		double alpha = homescreen_next_alpha(srv->homescreen, time);
		wlr_render_texture(renderer,
			homescreen_get_texture(srv->homescreen),
			out->transform_matrix, 0, 0,
			1.0f - alpha);
		wlr_render_texture(renderer,
			homescreen_get_applist_texture(srv->homescreen),
			out->transform_matrix, 0, 0,
			alpha);
		wlr_render_texture(renderer,
			topbar_get_texture(srv->topbar),
			out->transform_matrix, 0, 0, 1.0f);
	}
	render_navigation(srv, renderer, now, out, time);
}
