/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <wayland-server.h>
#include <wlr/types/wlr_text_input_v3.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <bananui/keys.h>
#include "text-input-protocol.h"
#include "server.h"
#include "output.h"
#include "topbar.h"
#include "textinput.h"

struct text_input_info {
	struct wl_listener text_input;
	struct wl_listener focused_destroy;
	struct wl_list inputs;
	struct wlr_surface *focused;
	struct input_data *active;
	struct server_data *server;
	int caps, nums, modelocked;
	char *keymap[10];
};
struct input_data {
	struct wlr_text_input_v3 *wlr_inp;
	struct wl_list link;
	struct text_input_info *tii;
	struct wl_listener enable;
	struct wl_listener commit;
	struct wl_listener disable;
	struct wl_listener destroy;
	int pending_key, key_index;
	char pending_char;
};

static void focused_destroy_notify(struct wl_listener *listener, void *data)
{
	/* used to avoid segfaults when the surface is gone */
	struct text_input_info *tii =
		wl_container_of(listener, tii, focused_destroy);
	tii->focused = NULL;
	fprintf(stderr, "text_input surface is gone!!!\n");
}

static void set_focused(struct text_input_info *tii, struct wlr_surface *surf)
{
	if(tii->focused){
		wl_list_remove(&tii->focused_destroy.link);
	}
	tii->focused = surf;
	if(surf){
		wl_signal_add(&surf->events.destroy, &tii->focused_destroy);
	}
}

static const char *input_info_string(struct text_input_info *tii)
{
	if(tii->nums) return "12";
	if(tii->caps) return "AB";
	return "ab";
}

static void text_input_enable_notify(struct wl_listener *listener, void *data)
{
	struct input_data *inp = wl_container_of(listener, inp, enable);
	struct text_input_info *tii = inp->tii;
	fprintf(stderr, "[DEBUG] Text input - Enable\n");
	if(tii->focused && inp->wlr_inp->focused_surface == tii->focused){
		tii->active = inp;
		switch(inp->wlr_inp->current.content_type.purpose){
			case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_NUMBER:
			case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_DIGITS:
			case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_PIN:
				tii->nums = 1;
				tii->modelocked = 1;
				break;
			default:
				tii->nums = 0;
				tii->modelocked = 0;
				break;
		}
		topbar_set_input_method(tii->server->topbar,
				input_info_string(tii));
		damage_app_output(tii->server);
	}
}
static void text_input_commit_notify(struct wl_listener *listener, void *data)
{
	fprintf(stderr, "[DEBUG] Text input - Commit\n");
}
static void text_input_disable_notify(struct wl_listener *listener, void *data)
{
	struct input_data *inp = wl_container_of(listener, inp, disable);
	struct text_input_info *tii = inp->tii;
	fprintf(stderr, "[DEBUG] Text input - Disable\n");
	if(tii->active == inp){
		topbar_set_input_method(tii->server->topbar, "");
		damage_app_output(tii->server);
		tii->active = NULL;
	}
}
static void text_input_destroy_notify(struct wl_listener *listener, void *data)
{
	struct input_data *inp = wl_container_of(listener, inp, destroy);
	struct text_input_info *tii = inp->tii;

	fprintf(stderr, "[DEBUG] Text input - Destroy\n");

	wl_list_remove(&inp->enable.link);
	wl_list_remove(&inp->commit.link);
	wl_list_remove(&inp->disable.link);
	wl_list_remove(&inp->destroy.link);
	wl_list_remove(&inp->link);

	if(tii->active == inp){
		topbar_set_input_method(tii->server->topbar, "");
		damage_app_output(tii->server);
		tii->active = NULL;
	}
	if(tii->focused && tii->focused == inp->wlr_inp->focused_surface)
		set_focused(tii, NULL);
}

static void text_input_notify(struct wl_listener *listener, void *data)
{
	struct text_input_info *tii =
		wl_container_of(listener, tii, text_input);
	struct input_data *inp;
	struct wlr_text_input_v3 *wlr_inp = data;
	fprintf(stderr, "[DEBUG] Text input\n");

	inp = malloc(sizeof(struct input_data));
	if(inp){
		inp->tii = tii;
		inp->wlr_inp = wlr_inp;
		inp->enable.notify = text_input_enable_notify;
		inp->commit.notify = text_input_commit_notify;
		inp->disable.notify = text_input_disable_notify;
		inp->destroy.notify = text_input_destroy_notify;
		wl_signal_add(&wlr_inp->events.enable, &inp->enable);
		wl_signal_add(&wlr_inp->events.commit, &inp->commit);
		wl_signal_add(&wlr_inp->events.disable, &inp->disable);
		wl_signal_add(&wlr_inp->events.destroy, &inp->destroy);
		wl_list_insert(&tii->inputs, &inp->link);

		inp->pending_key = -1;
		inp->key_index = -1;
	}

	if(tii->focused && wl_resource_get_client(tii->focused->resource) ==
		wl_resource_get_client(wlr_inp->resource))
	{
		wlr_text_input_v3_send_enter(wlr_inp, tii->focused);
	}
}

struct text_input_info *init_text_input(struct server_data *srv)
{
	struct text_input_info *tii;
	tii = malloc(sizeof(struct text_input_info));
	if(!tii) return NULL;
	tii->server = srv;
	tii->focused = NULL;
	tii->focused_destroy.notify = focused_destroy_notify;
	tii->active = NULL;
	tii->caps = tii->nums = tii->modelocked = 0;

	tii->keymap[0] = " 0";
	tii->keymap[1] = ".,!?1;:/@-+_=";
	tii->keymap[2] = "abc2";
	tii->keymap[3] = "def3";
	tii->keymap[4] = "ghi4";
	tii->keymap[5] = "jkl5";
	tii->keymap[6] = "mno6";
	tii->keymap[7] = "pqrs7";
	tii->keymap[8] = "tuv8";
	tii->keymap[9] = "wxyz9";

	wl_list_init(&tii->inputs);

	tii->text_input.notify = text_input_notify;
	wl_signal_add(&srv->txtmgr->events.text_input, &tii->text_input);

	return tii;
}

void text_input_focus(struct text_input_info *tii, struct wlr_surface *surf)
{
	struct input_data *inp;
	topbar_set_input_method(tii->server->topbar, "");
	damage_app_output(tii->server);
	tii->active = NULL;
	wl_list_for_each(inp, &tii->inputs, link){
		if(inp->wlr_inp->focused_surface &&
			inp->wlr_inp->focused_surface != surf)
		{
			fprintf(stderr, "[DEBUG] Text input - Sending leave\n");
			wlr_text_input_v3_send_leave(inp->wlr_inp);
		}
	}
	wl_list_for_each(inp, &tii->inputs, link){
		if(wl_resource_get_client(inp->wlr_inp->resource) ==
			wl_resource_get_client(surf->resource))
		{
			fprintf(stderr, "[DEBUG] Text input - Sending enter\n");
			wlr_text_input_v3_send_enter(inp->wlr_inp, surf);
		}
	}
	set_focused(tii, surf);
}

void text_input_unfocus(struct text_input_info *tii, struct wlr_surface *surf)
{
	struct input_data *inp;
	topbar_set_input_method(tii->server->topbar, "");
	damage_app_output(tii->server);
	tii->active = NULL;
	wl_list_for_each(inp, &tii->inputs, link){
		if(inp->wlr_inp->focused_surface == surf){
			fprintf(stderr, "[DEBUG] Text input - Sending leave\n");
			wlr_text_input_v3_send_leave(inp->wlr_inp);
		}
	}
	set_focused(tii, NULL);
}
static void text_input_complete_key(struct input_data *inp)
{
	if(inp->pending_key >= 0){
		char str[2];
		str[0] = inp->pending_char;
		str[1] = '\0';
		wlr_text_input_v3_send_commit_string(inp->wlr_inp, str);
		inp->pending_key = -1;
		inp->key_index = -1;
	}
}
static void text_input_nop_key(struct input_data *inp)
{
	text_input_complete_key(inp);
	wlr_text_input_v3_send_preedit_string(inp->wlr_inp, "", 0, 0);
	wlr_text_input_v3_send_done(inp->wlr_inp);
}
static void text_input_new_key(struct input_data *inp, int key)
{
	char str[2];
	if(inp->tii->nums){
		str[0] = '0' + key;
		str[1] = '\0';
		wlr_text_input_v3_send_commit_string(inp->wlr_inp, str);
	}
	else {
		if(key != inp->pending_key){
			text_input_complete_key(inp);
		}
		inp->pending_key = key;
		inp->key_index += 1;
		if('\0' == inp->tii->keymap[key][inp->key_index]){
			inp->key_index = 0;
		}
		inp->pending_char = inp->tii->keymap[key][inp->key_index];
		if(inp->tii->caps){
			inp->pending_char = toupper(inp->pending_char);
		}
		str[0] = inp->pending_char;
		str[1] = '\0';
		wlr_text_input_v3_send_preedit_string(inp->wlr_inp, str, 1, 1);
	}
	wlr_text_input_v3_send_done(inp->wlr_inp);
}

int text_input_handle_keypress(struct text_input_info *tii, xkb_keysym_t key)
{
	if(!tii->active) return 0;
	if(key == XKB_KEY_BackSpace && tii->active->pending_key >= 0){
		return 1;
	}
	else switch(key){
		case XKB_KEY_0: text_input_new_key(tii->active, 0); break;
		case XKB_KEY_1: text_input_new_key(tii->active, 1); break;
		case XKB_KEY_2: text_input_new_key(tii->active, 2); break;
		case XKB_KEY_3: text_input_new_key(tii->active, 3); break;
		case XKB_KEY_4: text_input_new_key(tii->active, 4); break;
		case XKB_KEY_5: text_input_new_key(tii->active, 5); break;
		case XKB_KEY_6: text_input_new_key(tii->active, 6); break;
		case XKB_KEY_7: text_input_new_key(tii->active, 7); break;
		case XKB_KEY_8: text_input_new_key(tii->active, 8); break;
		case XKB_KEY_9: text_input_new_key(tii->active, 9); break;
		default:
			text_input_nop_key(tii->active);
			return 0;
	}
	return 1;
}
int text_input_handle_keyrelease(struct text_input_info *tii, xkb_keysym_t key)
{
	if(!tii->active) return 0;

	if(key == XKB_KEY_BackSpace && tii->active->pending_key >= 0){
		tii->active->pending_key = -1;
		tii->active->key_index = -1;
		wlr_text_input_v3_send_preedit_string(
			tii->active->wlr_inp, "", 0, 0);
		wlr_text_input_v3_send_done(tii->active->wlr_inp);
		return 1;
	}
	if(key == BANANUI_KEY_Pound){
		if(!tii->modelocked){
			if(tii->nums) tii->nums = tii->caps = 0;
			else if(tii->caps) tii->nums = 1;
			else tii->caps = 1;
			topbar_set_input_method(tii->server->topbar,
				input_info_string(tii));
			damage_app_output(tii->server);
		}
		text_input_nop_key(tii->active);
	}
	return 0;
}
