/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <wlr/render/wlr_texture.h>
#include <bananui/widget.h>
#include "topbar.h"

struct topbar_data {
	cairo_surface_t *surf;
	cairo_t *cr;
	unsigned char *pixels;
	unsigned int height, width, stride;
	struct wlr_texture *texture;
	struct wlr_renderer *renderer;
	bBoxWidget *mainbox;
	bContentWidget *clock, *inputmethod, *inputicon;
};

static void topbar_repaint_into_texture(struct topbar_data *tb)
{
	if(tb->texture) wlr_texture_destroy(tb->texture);
	cairo_save(tb->cr);
	cairo_set_operator(tb->cr, CAIRO_OPERATOR_CLEAR);
	cairo_paint(tb->cr);
	cairo_restore(tb->cr);
	bShowMainBox(tb->surf, tb->cr, tb->mainbox, 0, 0);
	tb->texture = wlr_texture_from_pixels(tb->renderer,
		WL_SHM_FORMAT_ARGB8888,
		tb->stride, tb->width, tb->height, tb->pixels);
}

struct topbar_data *init_topbar(unsigned int width, unsigned int height,
		struct wlr_renderer *renderer)
{
	struct topbar_data *tb;
	tb = calloc(1, sizeof(struct topbar_data));
	if(!tb) return NULL;
	tb->renderer = renderer;
	tb->width = width;
	tb->height = height;
	tb->stride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32, width);

	tb->pixels = calloc(1, tb->stride * height);
	if(!tb->pixels) return NULL;

	tb->surf = cairo_image_surface_create_for_data(tb->pixels,
		CAIRO_FORMAT_ARGB32, width, height, tb->stride);
	if(!tb->surf) goto free_pixels;

	tb->cr = cairo_create(tb->surf);
	if(!tb->cr) goto free_surface;

	tb->mainbox = bCreateBoxWidget(B_ALIGN_CENTER,
			B_BOX_HBOX, -1, TOPBAR_HEIGHT);
	if(!tb->mainbox) goto err;


	tb->inputicon = bCreateContentWidget(B_ALIGN_START, 0);
	if(!tb->inputicon) goto free_box;
	bAddContentToBox(tb->mainbox, tb->inputicon);
	tb->inputicon->color = bRGBA(1.0, 1.0, 1.0, 0.4);
	bLoadImageFromIcon(tb->inputicon, "document-edit-symbolic", 16);

	tb->inputmethod = bCreateContentWidget(B_ALIGN_START, 0);
	if(!tb->inputmethod) goto free_box;
	bAddContentToBox(tb->mainbox, tb->inputmethod);
	tb->inputmethod->color = bRGBA(1.0, 1.0, 1.0, 1.0);
	bSetTextContent(tb->inputmethod, "", PANGO_WEIGHT_NORMAL, 17);

	bAddFillToBox(tb->mainbox);

	tb->clock = bCreateContentWidget(B_ALIGN_START, 0);
	if(!tb->clock) goto free_box;
	bAddContentToBox(tb->mainbox, tb->clock);
	tb->clock->color = bRGBA(1.0, 1.0, 1.0, 1.0);
	bSetTextContent(tb->clock, "Loading...", PANGO_WEIGHT_NORMAL, 17);

	tb->mainbox->color = bRGBA(0.0, 0.0, 0.0, 1.0);
	return tb;
free_box:
	bDestroyBoxRecursive(tb->mainbox);
err:
	cairo_destroy(tb->cr);
free_surface:
	cairo_surface_destroy(tb->surf);
free_pixels:
	free(tb->pixels);
	return NULL;
}

void topbar_set_color(struct topbar_data *tb,
	double r, double g, double b, double a)
{
	tb->mainbox->color = bRGBA(r, g, b, a);
	if(r == 1.0 && g == 1.0 && b == 1.0)
		tb->clock->color = bRGBA(0.0, 0.0, 0.0, 0.95);
	else
		tb->clock->color = bRGBA(1.0, 1.0, 1.0, 0.95);
	topbar_repaint_into_texture(tb);
}

void topbar_set_clock(struct topbar_data *tb, const char *time)
{
	bSetTextContent(tb->clock, time, PANGO_WEIGHT_NORMAL, 17);
	topbar_repaint_into_texture(tb);
}

void topbar_set_input_method(struct topbar_data *tb, const char *im)
{
	if(im[0] != '\0'){
		tb->inputicon->color = bRGBA(1.0, 1.0, 1.0, 1.0);
	}
	else {
		tb->inputicon->color = bRGBA(1.0, 1.0, 1.0, 0.5);
	}
	bSetTextContent(tb->inputmethod, im, PANGO_WEIGHT_NORMAL, 17);
	topbar_repaint_into_texture(tb);
}

struct wlr_texture *topbar_get_texture(struct topbar_data *tb)
{
	return tb->texture;
}
