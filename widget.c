/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <librsvg/rsvg.h>
#include <cairo.h>
#include <pango/pangocairo.h>
#include <assert.h>
#include <bananui/utils.h>
#include <bananui/widget.h>

const char *bGlobalAppClass = "system";

struct bsRsvgHandleWrapper {
	RsvgHandle *hd;
};

bBoxWidget *bCreateBoxWidget(bContentAlign align, bBoxDirection dir,
	double width, double height)
{
	bBoxWidget *box;
	box = malloc(sizeof(bBoxWidget));
	if(!box) return NULL;
	box->focusable = 0;
	box->clip = 0;
	box->self = NULL;
	box->focusin = box->focusout =
		box->click = box->skleft = box->skright = NULL;
	box->width = width;
	box->height = height;
	box->dir = dir;
	box->align = align;
	box->children.first = box->children.last = NULL;
	box->color.type = B_COLOR_TYPE_SIMPLE;
	box->color.simple.r = 0.0;
	box->color.simple.g = 0.0;
	box->color.simple.b = 0.0;
	box->color.simple.a = 0.0;
	box->mgn_l = box->mgn_t = box->mgn_r = box->mgn_b = 0;
	box->pad_l = box->pad_t = box->pad_r = box->pad_b = 0;
	return box;
}

bContentWidget *bCreateContentWidget(bContentAlign align, bContentFlags flags)
{
	bContentWidget *widget;
	widget = malloc(sizeof(bContentWidget));
	if(!widget) return NULL;
	widget->align = align;
	widget->self = NULL;
	widget->lt = NULL;
	widget->type = B_CONTENT_TYPE_EMPTY;
	widget->color.type = B_COLOR_TYPE_SIMPLE;
	widget->color.simple.r = 0;
	widget->color.simple.g = 0;
	widget->color.simple.b = 0;
	widget->color.simple.a = 1;
	widget->imgscale = 1.0;
	widget->svgwidth = 0.0;
	widget->svgheight = 0.0;
	widget->offset = 0.0;
	widget->flags = flags;
	widget->wrapmode = B_WRAP_WORD;
	widget->cursor = -1;
	widget->cursor_visible = 0;
	widget->font = NULL;
	widget->data = NULL;
	return widget;
}

int bAddBoxToBox(bBoxWidget *box, bBoxWidget *child)
{
	bWidgetListItem *tmp;
	tmp = malloc(sizeof(bWidgetListItem));
	if(!tmp) return 0;
	tmp->type = B_WIDGET_TYPE_BOX;
	tmp->box = child;
	tmp->prev = box->children.last;
	tmp->next = NULL;
	tmp->owner = box;
	if(box->children.last) box->children.last->next = tmp;
	else box->children.first = tmp;
	box->children.last = tmp;
	child->self = tmp;
	return 1;
}
int bAddContentToBox(bBoxWidget *box, bContentWidget *cont)
{
	bWidgetListItem *tmp;
	tmp = malloc(sizeof(bWidgetListItem));
	if(!tmp) return 0;
	tmp->type = B_WIDGET_TYPE_CONTENT;
	tmp->cont = cont;
	tmp->prev = box->children.last;
	tmp->next = NULL;
	tmp->owner = box;
	if(box->children.last) box->children.last->next = tmp;
	else box->children.first = tmp;
	box->children.last = tmp;
	cont->self = tmp;
	return 1;
}
int bAddFillToBox(bBoxWidget *box)
{
	bBoxWidget *child;
	child = bCreateBoxWidget(B_ALIGN_START, B_BOX_VBOX, -1, -1);
	if(!child) return 0;
	return bAddBoxToBox(box, child);
}

#ifdef BANANUI_LOG_WIDGETS
static const char *alignstr(bContentAlign align)
{
	switch(align){
		case B_ALIGN_START:
			return "Align at Start";
		case B_ALIGN_CENTER:
			return "Align at Center";
		case B_ALIGN_END:
			return "Align at End";
	}
	return "Align unknown";
}
#endif

static void showText(cairo_t *cr, bContentWidget *widget, bBoxWidget *box,
	double box_left, double box_top, double left, double top)
{
	double width, height;

	if(box->dir == B_BOX_HBOX){
		width = box->computed.width - (left - box_left);
		height = box->computed.height;
	}
	else {
		width = box->computed.width;
		height = box->computed.height - (top - box_top);
	}
	if(widget->cursor < 0){
		pango_layout_set_width(widget->lt, width*PANGO_SCALE);
	}
	if(widget->wrapmode != B_WRAP_ELLIPSIZE){
		pango_layout_set_height(widget->lt, height*PANGO_SCALE);
	}
	switch(widget->color.type){
		case B_COLOR_TYPE_SIMPLE:
			cairo_set_source_rgba(cr,
				widget->color.simple.r,
				widget->color.simple.g,
				widget->color.simple.b,
				widget->color.simple.a);
			break;
		default:
			break;
	}
	if(widget->cursor >= 0 && widget->cursor <= strlen(widget->text)){
		PangoRectangle strong, weak;
		double strong_x, strong_y, weak_x, weak_y;
		pango_layout_get_cursor_pos(widget->lt, widget->cursor,
			&strong, &weak);
		strong_x = strong.x / PANGO_SCALE;
		strong_y = strong.y / PANGO_SCALE;
		weak_x = weak.x / PANGO_SCALE;
		weak_y = weak.y / PANGO_SCALE;
		if(strong_x > (width - widget->offset)){
			widget->offset = width - strong_x;
		}
		if(strong_x < (-widget->offset)){
			widget->offset = -strong_x;
		}
#if BANANUI_VERBOSE
		fprintf(stderr, "[DEBUG] Render cursor at %f,%f %f\n",
			strong_x, strong_y, widget->offset);
#endif
		if(widget->cursor_visible){
			cairo_rectangle(cr,
				left + widget->offset + strong_x,
				top + strong_y,
				1,
				strong.height / PANGO_SCALE);
			cairo_rectangle(cr,
				left + widget->offset + weak_x,
				top + weak_y,
				1,
				weak.height / PANGO_SCALE);
			cairo_fill(cr);
		}
	}
	cairo_move_to(cr, left + widget->offset, top);
	pango_cairo_show_layout(cr, widget->lt);
	g_object_unref(widget->lt);
	widget->lt = NULL;
}

static void recolorSVG(bContentWidget *img, char **css)
{
	if(img->color.type != B_COLOR_TYPE_SIMPLE) return;
	*css = g_strdup_printf(
		"rect, path {\n"
		"  fill: rgba(%d, %d, %d, %d.%02d);\n"
		"}\n"
		".warning {\n"
		"  fill: rgba(%d, %d, %d, %d.%02d);\n"
		"}\n"
		".error {\n"
		"  fill: rgba(%d, %d, %d, %d.%02d);\n"
		"}\n"
		".success {\n"
		"  fill: rgba(%d, %d, %d, %d.%02d);\n"
		"}\n",
		(int)(img->color.simple.r * 255),
		(int)(img->color.simple.g * 255),
		(int)(img->color.simple.b * 255),
		(int)(img->color.simple.a),
		(int)(img->color.simple.a * 100) % 100,
		0xf5, 0x79, 0x00, 1, 0,
		0xff, 0x00, 0x00, 1, 0,
		0x33, 0xd1, 0x7a, 1, 0);
#if BANANUI_LOG_WIDGETS
	fprintf(stderr, "[DEBUG] Setting SVG style: %s\n", *css);
#endif
}

static void computeImageLayouts(bContentWidget *img)
{
	if(img->type == B_CONTENT_TYPE_IMAGE){
		img->computed.width =
			cairo_image_surface_get_width(img->imgsurf) *
			img->imgscale;
		img->computed.height =
			cairo_image_surface_get_height(img->imgsurf) *
			img->imgscale;
	}
	else if(img->type == B_CONTENT_TYPE_IMAGE_SVG){
		RsvgRectangle out, vp;
		RsvgLength iw, ih;
		gboolean has_width, has_height, has_vb;

		rsvg_handle_get_intrinsic_dimensions(img->rsvghandle->hd,
			&has_width, &iw, &has_height, &ih,
			&has_vb, &out);
		if(has_vb){
			/* output rectangle is set, nothing to do */
		}
		else if(has_width && has_height && iw.unit == RSVG_UNIT_PX &&
			ih.unit == RSVG_UNIT_PX)
		{
			out.width = iw.length;
			out.height = ih.length;
		}
		else {
			rsvg_handle_set_dpi(img->rsvghandle->hd, 90);
			vp.x = vp.y = 0;
			vp.width = img->svgwidth;
			vp.height = img->svgheight;
			if(!rsvg_handle_get_geometry_for_layer(
				img->rsvghandle->hd,
				NULL, &vp, NULL, &out, NULL))
			{
				fprintf(stderr, "Failed to get SVG geometry\n");
				img->computed.width = img->svgwidth;
				img->computed.height = img->svgheight;
				return;
			}
		}
		if(out.width > out.height){
			img->computed.width = img->svgwidth;
			img->computed.height = out.height /
				out.width * img->svgwidth;
		}
		else {
			img->computed.width = out.width /
				out.height * img->svgheight;
			img->computed.height = img->svgheight;
		}
#if BANANUI_LOG_WIDGETS
		fprintf(stderr,
			"[DEBUG] Width:desired=%f,actual=%f\n",
			img->svgwidth, img->computed.width);
		fprintf(stderr, "[DEBUG] Height:desired=%f,actual=%f\n",
			img->svgheight, img->computed.height);
#endif
	}
}

static void box_get_width(bBoxWidget *box, cairo_surface_t *surf, cairo_t *cr,
	int dont_descend);
static void box_get_height(bBoxWidget *box, cairo_surface_t *surf, cairo_t *cr,
	int dont_descend);

static void computeTextLayouts(bContentWidget *text, cairo_surface_t *surf,
	cairo_t *cr)
{
	PangoRectangle extents;
	if(text->lt) g_object_unref(text->lt);
	text->lt = pango_cairo_create_layout(cr);
	pango_layout_set_font_description(text->lt,
		text->font);
	pango_layout_set_text(text->lt, text->text,
		-1);
	switch(text->align){
		case B_ALIGN_START:
			pango_layout_set_alignment(
				text->lt,
				PANGO_ALIGN_LEFT);
			break;
		case B_ALIGN_CENTER:
			pango_layout_set_alignment(
				text->lt,
				PANGO_ALIGN_CENTER);
			break;
		case B_ALIGN_END:
			pango_layout_set_alignment(
				text->lt,
				PANGO_ALIGN_RIGHT);
			break;
	}
	box_get_width(text->self->owner, surf, cr, 1);
	box_get_height(text->self->owner, surf, cr, 1);
	if(text->self->owner->computed.width >= 0 && text->cursor < 0){
		pango_layout_set_width(text->lt,
			text->self->owner->computed.width *
			PANGO_SCALE);
	}
	if(text->wrapmode == B_WRAP_ELLIPSIZE){
		pango_layout_set_ellipsize(text->lt,
			PANGO_ELLIPSIZE_END);
		pango_layout_set_height(text->lt, -1);
	}
	else if(text->self->owner->computed.height >= 0){
		pango_layout_set_ellipsize(text->lt,
			PANGO_ELLIPSIZE_END);
		pango_layout_set_height(text->lt,
			text->self->owner->computed.height *
			PANGO_SCALE);
	}

	pango_layout_set_wrap(text->lt,
		text->wrapmode == B_WRAP_BREAK ? PANGO_WRAP_WORD_CHAR :
		PANGO_WRAP_WORD);

	pango_layout_get_pixel_extents(text->lt, NULL, &extents);
	text->computed.width = extents.width;
	text->computed.height = extents.height;
}

static void cont_get_width(bContentWidget *cont, cairo_surface_t *surf,
	cairo_t *cr)
{
	if(cont->computed.width >= 0) return;
	if(cont->type == B_CONTENT_TYPE_TEXT){
		computeTextLayouts(cont, surf, cr);
	}
	else if(cont->type == B_CONTENT_TYPE_IMAGE ||
		cont->type == B_CONTENT_TYPE_IMAGE_SVG)
	{
		computeImageLayouts(cont);
	}
}

static void cont_get_height(bContentWidget *cont, cairo_surface_t *surf,
	cairo_t *cr)
{
	if(cont->computed.height >= 0) return;
	if(cont->type == B_CONTENT_TYPE_TEXT){
		computeTextLayouts(cont, surf, cr);
	}
	else if(cont->type == B_CONTENT_TYPE_IMAGE ||
		cont->type == B_CONTENT_TYPE_IMAGE_SVG)
	{
		computeImageLayouts(cont);
	}
}

/* These macros are hard to debug, but I don't see a way to get around them
 * without too much code duplication
 * TODO: Maybe try using struct offsets?
 */

#define BOX_GET_DIM_MIN(box, _dim, _dir, _dont_descend) do { \
	bWidgetListItem *_tmp; \
	double _acc = 0; \
	for(_tmp = (box)->children.first; _tmp; _tmp = _tmp->next){ \
		double _size = 0; \
		if(_tmp->type == B_WIDGET_TYPE_BOX){ \
			box_get_ ## _dim (_tmp->box, surf, cr, 0); \
			_size = _tmp->box->computed._dim; \
			_size += \
				(box)->dir == B_BOX_HBOX ? \
				_tmp->box->mgn_l : \
				_tmp->box->mgn_t; \
			_size += \
				(box)->dir == B_BOX_HBOX ? \
				_tmp->box->mgn_r : \
				_tmp->box->mgn_b; \
		} \
		else if(_tmp->type == B_WIDGET_TYPE_CONTENT){ \
			cont_get_ ## _dim (_tmp->cont, surf, cr); \
			_size = _tmp->cont->computed._dim; \
		} \
		if((box)->dir == _dir){ \
			_acc += _size; \
		} \
		else { \
			if(_size > _acc) _acc = _size; \
		} \
	} \
	(box)->computed._dim = _acc; \
} while(0)

#define BOX_GET_DIM_STRETCH(box, _dim, _dir, _dont_descend) do { \
	bBoxWidget *_par = NULL; \
	if((box)->self && (box)->self->owner) _par = (box)->self->owner; \
	if(!_par){ \
		(box)->computed._dim = auto_surface_get_ ## _dim (surf); \
	} \
	else if(_par->dir == _dir){ \
		if(_par->computed.num_stretches < 0 || \
			_par->computed.used_space < 0) \
		{ \
			bWidgetListItem *_tmp; \
			_par->computed.num_stretches = 0; \
			_par->computed.used_space = 0; \
			for(_tmp = _par->children.first; _tmp; \
				_tmp = _tmp->next) \
			{ \
				if(_tmp->type == B_WIDGET_TYPE_BOX && \
					_tmp->box->_dim < 0) \
				{ \
					_par->computed.num_stretches++; \
					_par->computed.used_space += \
						_dir == B_BOX_HBOX ? \
						_tmp->box->mgn_l : \
						_tmp->box->mgn_t; \
					_par->computed.used_space += \
						_dir == B_BOX_HBOX ? \
						_tmp->box->mgn_r : \
						_tmp->box->mgn_b; \
				} \
				else if(_tmp->type == B_WIDGET_TYPE_BOX && \
					_tmp->box->_dim >= 0) \
				{ \
					box_get_ ## _dim ( \
						_tmp->box, surf, cr, 0); \
					_par->computed.used_space += \
						_tmp->box->computed._dim; \
					_par->computed.used_space += \
						_dir == B_BOX_HBOX ? \
						_tmp->box->mgn_l : \
						_tmp->box->mgn_t; \
					_par->computed.used_space += \
						_dir == B_BOX_HBOX ? \
						_tmp->box->mgn_r : \
						_tmp->box->mgn_b; \
				} \
				else if(_tmp->type == B_WIDGET_TYPE_CONTENT){ \
					cont_get_ ## _dim ( \
						_tmp->cont, surf, cr); \
					_par->computed.used_space += \
						_tmp->cont->computed._dim; \
				} \
			} \
		} \
		assert(_par->computed.num_stretches > 0); \
		box_get_ ## _dim (_par, surf, cr, 1); \
		if(_par->computed._dim >= 0){ \
			(box)->computed._dim = \
				(_par->computed._dim - \
				 _par->computed.used_space) / \
				_par->computed.num_stretches; \
		} \
		else if(!_dont_descend){ \
			BOX_GET_DIM_MIN(box, _dim, _dir, _dont_descend); \
		} \
		else { \
			(box)->computed._dim = -1; \
		} \
	} \
	else if(_par->dir != _dir){ \
		box_get_ ## _dim (_par, surf, cr, 1); \
		if(_par->computed._dim >= 0){ \
			(box)->computed._dim = _par->computed._dim; \
			if(_par->dir == B_BOX_HBOX){ \
				(box)->computed._dim -= \
					(box)->mgn_t; \
				(box)->computed._dim -= \
					(box)->mgn_b; \
			} \
			else { \
				(box)->computed._dim -= \
					(box)->mgn_l; \
				(box)->computed._dim -= \
					(box)->mgn_r; \
			} \
		} \
		else if(!_dont_descend){ \
			BOX_GET_DIM_MIN(box, _dim, _dir, _dont_descend); \
		} \
		else { \
			(box)->computed._dim = -1; \
		} \
	} \
} while(0)


static void box_get_width(bBoxWidget *box, cairo_surface_t *surf, cairo_t *cr,
	int dont_descend)
{
	if(box->computed.width >= 0) return;
	if(box->width > 0){
		box->computed.width = box->width;
	}
	else if(box->width == 0 && dont_descend){
		box->computed.width = -1;
	}
	else if(box->width == 0){
		BOX_GET_DIM_MIN(box, width, B_BOX_HBOX, dont_descend);
	}
	else if(box->width < 0){
		BOX_GET_DIM_STRETCH(box, width, B_BOX_HBOX, dont_descend);
	}
}

static void box_get_height(bBoxWidget *box, cairo_surface_t *surf, cairo_t *cr,
	int dont_descend)
{
	if(box->computed.height >= 0) return;
	if(box->height > 0){
		box->computed.height = box->height;
	}
	else if(box->height == 0 && dont_descend){
		box->computed.height = -1;
	}
	else if(box->height == 0){
		BOX_GET_DIM_MIN(box, height, B_BOX_VBOX, dont_descend);
	}
	else if(box->height < 0){
		BOX_GET_DIM_STRETCH(box, height, B_BOX_VBOX, dont_descend);
	}
}

static void resetLayout(bBoxWidget *box)
{
	bWidgetListItem *tmp;
	box->computed.width = box->computed.height = -1;
	box->computed.used_space = -1;
	box->computed.num_stretches = -1;
	box->computed.top = box->computed.left = 0;
	for(tmp = box->children.first; tmp; tmp = tmp->next){
		if(tmp->type == B_WIDGET_TYPE_BOX){
			resetLayout(tmp->box);
		}
		else if(tmp->type == B_WIDGET_TYPE_CONTENT){
			tmp->cont->computed.width = tmp->cont->computed.height =
				-1;
			tmp->cont->computed.top = tmp->cont->computed.left =
				-1;
		}
	}
}

static int showMainBox_recursive(cairo_surface_t *surf,
	cairo_t *cr, bBoxWidget *box, int indent, double top, double left)
{
	bWidgetListItem *tmp;
	double box_top=top, box_left=left;
#if BANANUI_LOG_WIDGETS
	int i;
#endif

	box->computed.top = top;
	box->computed.left = left;

	box_get_width(box, surf, cr, 0);
	box_get_height(box, surf, cr, 0);

#if BANANUI_LOG_WIDGETS
	for(i = 0; i < indent; i++) fputc(' ', stderr);
	fprintf(stderr, "Box (%s) w=%lf h=%lf at (%lf,%lf) {\n",
		alignstr(box->align),
		box->computed.width, box->computed.height,
		top, left);
	indent++;
#endif
#if BANANUI_OUTLINE_BOX
	cairo_set_source_rgba(cr, 1, 0, 1, 1);
	cairo_rectangle(cr, left, top,
		box->computed.width,
		box->computed.height);
	cairo_stroke(cr);
#endif
	if(box->clip){
		cairo_save(cr);
		cairo_rectangle(cr, left, top,
			box->computed.width,
			box->computed.height);
		cairo_clip(cr);
	}

	switch(box->color.type){
		case B_COLOR_TYPE_SIMPLE:
			if(box->color.simple.a == 0) break;
			cairo_set_source_rgba(cr,
				box->color.simple.r,
				box->color.simple.g,
				box->color.simple.b,
				box->color.simple.a);
			cairo_rectangle(cr, left, top,
				box->computed.width,
				box->computed.height);
			cairo_fill(cr);
			break;
		default:
			break;
	}

	for(tmp = box->children.first; tmp; tmp = tmp->next)
	{
		if(tmp->type == B_WIDGET_TYPE_BOX){
			double align = 0;
			box_get_height(tmp->box, surf, cr, 0);
			box_get_width(tmp->box, surf, cr, 0);
			if(tmp->box->align != B_ALIGN_START){
				align = box->dir == B_BOX_HBOX ?
					(box->computed.height -
					 tmp->box->mgn_b -
					 tmp->box->computed.height) :
					(box->computed.width -
					 tmp->box->mgn_r -
					 tmp->box->computed.width);
				if(tmp->box->align == B_ALIGN_CENTER){
					align += box->dir == B_BOX_HBOX ?
						tmp->box->mgn_t :
						tmp->box->mgn_l;
					align /= 2;
				}
			}
			else {
				if(box->dir == B_BOX_VBOX){
					align += tmp->box->mgn_l;
				}
				else {
					align += tmp->box->mgn_t;
				}
			}
			if(box->dir == B_BOX_VBOX){
				top += tmp->box->mgn_t;
			}
			else {
				left += tmp->box->mgn_l;
			}
			showMainBox_recursive(surf, cr, tmp->box, indent,
				box->dir == B_BOX_HBOX ? top+align : top,
				box->dir == B_BOX_VBOX ? left+align : left);
			if(box->dir == B_BOX_VBOX){
				top += tmp->box->computed.height;
				top += tmp->box->mgn_b;
			}
			else {
				left += tmp->box->computed.width;
				left += tmp->box->mgn_r;
			}
		}
		else if(tmp->type == B_WIDGET_TYPE_CONTENT){
			double align = 0;
			cont_get_width(tmp->cont, surf, cr);
			cont_get_height(tmp->cont, surf, cr);
			if(box->dir == B_BOX_HBOX &&
				tmp->cont->align != B_ALIGN_START)
			{
				align = box->computed.height -
					 tmp->cont->computed.height;
				if(tmp->cont->align == B_ALIGN_CENTER){
					align /= 2;
				}
			}
#if BANANUI_LOG_WIDGETS
			for(i = 0; i < indent; i++) fputc(' ', stderr);
#endif
			if(tmp->cont->type == B_CONTENT_TYPE_TEXT){
#if BANANUI_LOG_WIDGETS
				fprintf(stderr,
					"Text (%s) w=%lf h=%lf \"%s\"\n",
					alignstr(tmp->cont->align),
					tmp->cont->computed.width,
					tmp->cont->computed.height,
					tmp->cont->text);
#endif
				tmp->cont->computed.left = left;
				tmp->cont->computed.top = top+align;
				if(tmp->cont->lt){
					showText(cr, tmp->cont, box,
						box_left, box_top,
						left, top+align);
				}
			}
			else if(tmp->cont->type == B_CONTENT_TYPE_IMAGE){
				if(box->dir == B_BOX_VBOX &&
					tmp->cont->align != B_ALIGN_START)
				{
					align = box->computed.width -
						 tmp->cont->computed.width;
					if(tmp->cont->align == B_ALIGN_CENTER){
						align /= 2;
					}
				}
				cairo_save(cr);
				cairo_scale(cr,
					tmp->cont->imgscale,
					tmp->cont->imgscale);
				tmp->cont->computed.left =
					(box->dir == B_BOX_VBOX ?
					left+align : left);
				tmp->cont->computed.top =
					(box->dir == B_BOX_HBOX ?
					top+align : top);
				cairo_set_source_surface(cr, tmp->cont->imgsurf,
					tmp->cont->computed.left /
					tmp->cont->imgscale,
					tmp->cont->computed.top /
					tmp->cont->imgscale);
				cairo_paint(cr);
				cairo_restore(cr);
			}
			else if(tmp->cont->type == B_CONTENT_TYPE_IMAGE_SVG){
				RsvgRectangle vp;
				char *style = NULL;
				GError *err = NULL;
				if(tmp->cont->svgrecolor)
					recolorSVG(tmp->cont, &style);
				if(style && !rsvg_handle_set_stylesheet(
					tmp->cont->rsvghandle->hd,
					(unsigned char*)style,
					strlen(style), &err))
				{
					if(err) fprintf(stderr,
						"SVG set style failed: %s\n",
						err->message);
				}
				if(style) free(style);
				if(box->dir == B_BOX_VBOX &&
					tmp->cont->align != B_ALIGN_START)
				{
					align = box->computed.width -
						 tmp->cont->computed.width;
					if(tmp->cont->align == B_ALIGN_CENTER){
						align /= 2;
					}
				}
				if(box->dir == B_BOX_VBOX){
					vp.x = left+align;
					vp.y = top;
				}
				else {
					vp.x = left;
					vp.y = top+align;
				}
				tmp->cont->computed.top = vp.y;
				tmp->cont->computed.left = vp.x;
				vp.width = tmp->cont->computed.width;
				vp.height = tmp->cont->computed.height;
				rsvg_handle_render_document(
					tmp->cont->rsvghandle->hd, cr,
					&vp, NULL);
			}
			if(box->dir == B_BOX_VBOX){
				top += tmp->cont->computed.height;
			}
			else {
				left += tmp->cont->computed.width;
			}
		}
#if BANANUI_LOG_WIDGETS
		else {
			for(i = 0; i < indent; i++) fputc(' ', stderr);
			fprintf(stderr, "!! Unknown widget !!\n");
		}
#endif
	}
	indent--;
#if BANANUI_LOG_WIDGETS
	for(i = 0; i < indent; i++) fputc(' ', stderr);
	fprintf(stderr, "}\n");
#endif
	if(box->clip){
		cairo_restore(cr);
	}
	return 1;
}
static int computeLayout_recursive(cairo_surface_t *surf, cairo_t *cr,
	bBoxWidget *box)
{
	bWidgetListItem *tmp;
	box_get_width(box, surf, cr, 0);
	box_get_height(box, surf, cr, 0);

	for(tmp = box->children.first; tmp; tmp = tmp->next){
		if(tmp->type == B_WIDGET_TYPE_BOX){
			bComputeLayout(surf, cr, tmp->box);
		}
		else if(tmp->type == B_WIDGET_TYPE_CONTENT){
			cont_get_width(tmp->cont, surf, cr);
			cont_get_height(tmp->cont, surf, cr);
		}
	}
	return 1;
}

int bComputeLayout(cairo_surface_t *surf, cairo_t *cr, bBoxWidget *box)
{
	resetLayout(box);
	return computeLayout_recursive(surf, cr, box);
}

int bShowMainBox(cairo_surface_t *surf, cairo_t *cr, bBoxWidget *box,
	double top, double left)
{
	resetLayout(box);
	return showMainBox_recursive(surf, cr, box, 0, top, left);
}

static void freeCont(bContentWidget *cont)
{
	switch(cont->type){
		case B_CONTENT_TYPE_TEXT:
			if(cont->font) pango_font_description_free(cont->font);
			if(cont->text) free(cont->text);
			break;
		case B_CONTENT_TYPE_IMAGE:
			if(cont->imgsurf) cairo_surface_destroy(cont->imgsurf);
			break;
		case B_CONTENT_TYPE_IMAGE_SVG:
			if(cont->rsvghandle){
				if(cont->rsvghandle->hd){
					g_object_unref(cont->rsvghandle->hd);
				}
				free(cont->rsvghandle);
			}
			break;
		case B_CONTENT_TYPE_EMPTY:
			break;
	}
}

static GKeyFile *loadThemeFile(void)
{
	static GKeyFile *theme = NULL;
	char *themepath, *dirpath;
	GError *err = NULL;

	if(theme) return theme;

	themepath = g_strdup_printf("%s/bananui/theme.ini",
			g_get_user_config_dir());
	theme = g_key_file_new();
	if(!theme) goto free_path;
	if(!g_key_file_load_from_file(theme, themepath,
			G_KEY_FILE_NONE, &err))
	{
		g_key_file_set_string(theme, "Metadata", "Inherits", "default");
		/* ignore save errors */
		dirpath = strdup(themepath);
		if(dirpath){
			dirpath[strlen(dirpath) - strlen("/theme.ini")] = '\0';
			mkdir(dirpath, 0777);
			free(dirpath);
		}
		g_key_file_save_to_file(theme, themepath, NULL);
	}
	free(themepath);
	return theme;
free_path:
	free(themepath);
	return NULL;
}

static GKeyFile *loadSystemTheme(const char *name)
{
	GKeyFile *theme;
	const char * const *datadirs;
	char *themepath = NULL;

	theme = g_key_file_new();
	if(!theme) return NULL;

	datadirs = g_get_system_data_dirs();

	for(; *datadirs; datadirs++){
		themepath = g_strdup_printf("%s/bananui/themes/%s.ini",
				*datadirs, name);
#if BANANUI_THEME_SEARCH_LOG
		fprintf(stderr, "[DEBUG] Checking for theme file %s\n",
				themepath);
#endif
		if(g_key_file_load_from_file(theme, themepath,
				G_KEY_FILE_NONE, NULL))
		{
			return theme;
		}
		free(themepath);
		themepath = NULL;
	}
	if(!themepath){
		themepath = g_strdup_printf("%s/bananui/themes/%s.ini",
				g_get_user_data_dir(), name);
#if BANANUI_THEME_SEARCH_LOG
		fprintf(stderr, "[DEBUG] Checking for theme file %s\n",
				themepath);
#endif
		if(g_key_file_load_from_file(theme, themepath,
				G_KEY_FILE_NONE, NULL))
		{
			return theme;
		}
	}
	g_key_file_unref(theme);
	return NULL;
}

struct bananui_theme {
	GKeyFile *kf;
	struct bananui_theme *parent;
};

static struct bananui_theme *loadTheme(void)
{
	static struct bananui_theme *theme;

	if(theme) return theme;

	theme = malloc(sizeof(struct bananui_theme));
	if(!theme) return NULL;

	theme->parent = NULL;
	theme->kf = loadThemeFile();
	if(!theme->kf) goto free_theme;

	return theme;

free_theme:
	free(theme);
	theme = NULL;
	return NULL;
}

static char *themeSearchString(struct bananui_theme *theme,
		const char *section, const char *name)
{
	for(; theme; theme = theme->parent){
		char *str;
		str = g_key_file_get_string(theme->kf, section, name, NULL);
		if(str) return str;
		if(!theme->parent){
			str = g_key_file_get_string(theme->kf,
				"Metadata", "Inherits", NULL);
			if(!str) break;
			theme->parent = malloc(sizeof(struct bananui_theme));
			if(!theme->parent){
				free(str);
				break;
			}
			theme->parent->parent = NULL;
			theme->parent->kf = loadSystemTheme(str);
			if(!theme->parent->kf){
				free(theme->parent);
				theme->parent = NULL;
			}
			free(str);
		}
	}
	return NULL;
}

int bSetTextContent(bContentWidget *cont, const char *txt,
	PangoWeight weight, double size)
{
	char *tmp, *fontname = NULL;
	struct bananui_theme *theme;
	tmp = strdup(txt);
	if(!tmp) return 0;
	freeCont(cont);
	cont->font = pango_font_description_new();
	if(!cont->font){
		free(tmp);
		return 0;
	}
	theme = loadTheme();
	if(theme){
		fontname = themeSearchString(theme, "Metadata", "Font");
	}
	pango_font_description_set_family(cont->font,
		fontname ? fontname : "Open Sans");
	pango_font_description_set_weight(cont->font, weight);
	pango_font_description_set_absolute_size(cont->font,
		PANGO_SCALE*size);
	cont->type = B_CONTENT_TYPE_TEXT;
	cont->text = tmp;
	return 1;
}

static int stringEndsWith(const char *str, const char *end, size_t len)
{
	size_t endlen;
	endlen = strlen(end);
	if(len < endlen) return 0;
	while(endlen){
		if(str[len - endlen] != *end) return 0;
		endlen--;
		end++;
	}
	return 1;
}

int bLoadImageFromFile(bContentWidget *cont, const char *filename)
{
	cairo_surface_t *tmp;
	size_t filenamelen;
	filenamelen = strlen(filename);
	if(stringEndsWith(filename, ".svg", filenamelen)){
		RsvgHandle *hd;
		freeCont(cont);
		cont->type = B_CONTENT_TYPE_IMAGE_SVG;
		cont->rsvghandle = malloc(sizeof(bRsvgHandleWrapper));
		if(!cont->rsvghandle) return 0;
		hd = rsvg_handle_new_from_file(filename, NULL);
		if(!hd){
			free(cont->rsvghandle);
			return 0;
		}
		cont->rsvghandle->hd = hd;
		cont->svgrecolor = 0;
	}
	else if(stringEndsWith(filename, ".png", filenamelen)){
		tmp = cairo_image_surface_create_from_png(filename);
		if(!tmp) return 0;
		freeCont(cont);
		cont->type = B_CONTENT_TYPE_IMAGE;
		cont->imgsurf = tmp;
	}
	else return 0;
	cont->imgscale = 1;
	return 1;
}

struct icon_theme {
	struct icon_directory *dirs;
	struct icon_theme *next;
};
struct icon_directory {
	char *path;
	int minsize, maxsize, scalable;
	struct icon_directory *next;
};

static struct icon_theme *iconThemes;
static const char *suffixes[] = {
	"svg",
	"png",
	NULL
};

static struct icon_theme *loadIconThemes(const char *name)
{
	GKeyFile *kf;
	const char * const *datadirs;
	const char * const *datadir;
	char **directories;
	char *homedir, *filename = NULL, *inherits;
	struct icon_theme *thm;
	struct icon_directory *tmp = NULL, **dest;
	int isFallback = (strcmp(name, "hicolor") == 0);
	thm = malloc(sizeof(struct icon_theme));
	if(!thm) return NULL;
	kf = g_key_file_new();
	if(!kf){
		free(thm);
		return NULL;
	}
	homedir = getenv("HOME");
	if(homedir){
		filename = g_strdup_printf("%s/.icons/%s/index.theme",
			homedir, name);
#if BANANUI_ICON_SEARCH_LOG
		fprintf(stderr,
			"[DEBUG] Checking for icontheme %s...\n",
			filename);
#endif
		if(!g_key_file_load_from_file(kf, filename,
			G_KEY_FILE_NONE, NULL))
		{
			free(filename);
			filename = NULL;
		}
	}
	datadirs = g_get_system_data_dirs();
	if(!filename){
		for(datadir = datadirs; *datadir; datadir++){
			filename = g_strdup_printf("%s/icons/%s/index.theme",
				*datadir, name);
#if BANANUI_ICON_SEARCH_LOG
			fprintf(stderr,
				"[DEBUG] Checking for icontheme %s...\n",
				filename);
#endif
			if(g_key_file_load_from_file(kf, filename,
				G_KEY_FILE_NONE, NULL))
			{
				break;
			}
			free(filename);
			filename = NULL;
		}
	}
	if(!filename){
		filename = g_strdup_printf("%s/icons/%s/index.theme",
			g_get_user_data_dir(), name);
#if BANANUI_ICON_SEARCH_LOG
		fprintf(stderr,
			"[DEBUG] Checking for icontheme %s...\n",
			filename);
#endif
		if(!g_key_file_load_from_file(kf, filename,
			G_KEY_FILE_NONE, NULL))
		{
			free(filename);
			g_key_file_unref(kf);
			return NULL;
		}
	}
	/* Leave out the /index.theme at the end so that we can append directory
	 * names */
	filename[strlen(filename) - strlen("/index.theme")] = '\0';
	inherits = g_key_file_get_string(kf, "Icon Theme", "Inherits", NULL);
	if(inherits){
		thm->next = loadIconThemes(inherits);
		free(inherits);
	}
	else if(!isFallback){
		thm->next = loadIconThemes("hicolor");
	}
	else {
		thm->next = NULL;
	}
	g_key_file_set_list_separator(kf, ',');
	directories = g_key_file_get_string_list(kf, "Icon Theme",
			"Directories", NULL, NULL);
	thm->dirs = NULL;
	if(directories){
		struct icon_directory *last = NULL;
		char **directory;
		for(directory = directories; *directory; directory++){
			char *type;
			int size;

			tmp = malloc(sizeof(struct icon_directory));
			if(!tmp) break;
			tmp->next = NULL;
			tmp->path = g_strdup_printf("%s/%s", filename,
					*directory);
			type = g_key_file_get_string(kf,
					*directory, "Type", NULL);
			size = g_key_file_get_integer(kf,
					*directory, "Size", NULL);
			tmp->minsize = g_key_file_get_integer(kf,
					*directory, "MinSize", NULL);
			tmp->maxsize = g_key_file_get_integer(kf,
					*directory, "MaxSize", NULL);
			if(tmp->minsize == 0) tmp->minsize = size;
			if(tmp->maxsize == 0) tmp->maxsize = size;
			if(type && 0 == strcmp(type, "Threshold")){
				tmp->minsize = size - 2;
				tmp->maxsize = size + 2;
			}
			if(last) last->next = tmp;
			if(!thm->dirs) thm->dirs = tmp;
			if(type){
				tmp->scalable = (0 == strcmp(type, "Scalable"));
				free(type);
			}
			else {
				tmp->scalable = 0;
			}
			last = tmp;
		}
		g_strfreev(directories);
	}
	dest = tmp ? &(tmp->next) : &(thm->dirs);

	/* Search for icons at the top level of the pixmaps / icons directory */
	/* The sizes are set to INT_MAX since they are the least preferred
	 * icons */
	if(homedir){
		(*dest) = malloc(sizeof(struct icon_directory));
		if(*dest){
			(*dest)->next = NULL;
			(*dest)->path = g_strdup_printf("%s/.icons", homedir);
			(*dest)->minsize = INT_MAX;
			(*dest)->maxsize = INT_MAX;
			(*dest)->scalable = 0;
			dest = &((*dest)->next);
		}
	}
	if(isFallback){
		for(datadir = datadirs; *datadir; datadir++){
			(*dest) = malloc(sizeof(struct icon_directory));
			if(!*dest) break;
			(*dest)->next = NULL;
			(*dest)->minsize = INT_MAX;
			(*dest)->maxsize = INT_MAX;
			(*dest)->scalable = 0;
			(*dest)->path = g_strdup_printf("%s/icons", *datadir);
			dest = &((*dest)->next);
			(*dest) = malloc(sizeof(struct icon_directory));
			if(!*dest) break;
			(*dest)->next = NULL;
			(*dest)->minsize = INT_MAX;
			(*dest)->maxsize = INT_MAX;
			(*dest)->scalable = 0;
			(*dest)->path = g_strdup_printf("%s/pixmaps", *datadir);
			dest = &((*dest)->next);
		}
		(*dest) = malloc(sizeof(struct icon_directory));
		if(*dest){
			(*dest)->next = NULL;
			(*dest)->minsize = INT_MAX;
			(*dest)->maxsize = INT_MAX;
			(*dest)->scalable = 0;
			(*dest)->path = g_strdup_printf("%s/icons",
					g_get_user_data_dir());
			dest = &((*dest)->next);
		}
		(*dest) = malloc(sizeof(struct icon_directory));
		if(*dest){
			(*dest)->next = NULL;
			(*dest)->minsize = INT_MAX;
			(*dest)->maxsize = INT_MAX;
			(*dest)->scalable = 0;
			(*dest)->path = g_strdup_printf("%s/pixmaps",
					g_get_user_data_dir());
			dest = &((*dest)->next);
		}
	}
	free(filename);
	g_key_file_unref(kf);
	return thm;
}

static char *getIconFilename(const char *name, int size)
{
	struct icon_theme *thm;
	struct icon_directory *dir;
	char *best_path = NULL;
	int best_sizediff = INT_MAX, best_scalable = 0;
	if(!iconThemes){
		char *theme_name = NULL;
		struct bananui_theme *b_theme = loadTheme();
		if(b_theme){
			theme_name = themeSearchString(b_theme, "Metadata",
					"IconTheme");
		}
		iconThemes = loadIconThemes(theme_name ?
				theme_name : "Adwaita");
	}
	for(thm = iconThemes; thm; thm = thm->next){
#if BANANUI_ICON_SEARCH_LOG
		fprintf(stderr, "[DEBUG] Looking for %s in a theme\n", name);
#endif
		for(dir = thm->dirs; dir; dir = dir->next){
			char *path;
			const char **suffix = suffixes;
			size_t maxpath;
			int sizediff, fd;
#if BANANUI_ICON_SEARCH_LOG_VERBOSE
			fprintf(stderr, "[DEBUG] Looking for %s in %s\n",
				name, dir->path);
#endif
			if(size < dir->minsize){
				sizediff = dir->minsize - size;
			}
			else if(size > dir->maxsize){
				sizediff = size - dir->maxsize;
			}
			else {
				sizediff = 0;
			}
			if(sizediff < best_sizediff ||
				(!dir->scalable && best_scalable &&
				 sizediff == best_sizediff))
			{
				maxpath = strlen(dir->path) + 1 +
					strlen(name) +
					strlen(".symbolic.png"
						/* the longest suffix */) + 1;
				path = malloc(maxpath);
				do {
					snprintf(path, maxpath, "%s/%s.%s",
						dir->path, name, *suffix);
					suffix++;
					fd = open(path, O_RDONLY);
				} while(fd < 0 && *suffix);
				if(fd > 0) {
					close(fd);
					best_scalable = dir->scalable;
					best_sizediff = sizediff;
					best_path = path;
				}
				else {
					free(path);
				}
			}
		}
	}
	return best_path;
}

int bLoadImageFromIcon(bContentWidget *cont, const char *name, int size)
{
	double width, height;
	if(name[0] != '/'){
		char *filename = getIconFilename(name, size);
		if(!filename) return 0;
#if BANANUI_ICON_SEARCH_LOG
		fprintf(stderr, "[DEBUG] Icon file: %s\n", filename);
#endif
		if(!bLoadImageFromFile(cont, filename)){
			free(filename);
			return 0;
		}
		free(filename);
	}
	else {
#if BANANUI_ICON_SEARCH_LOG
		fprintf(stderr, "[DEBUG] Icon file: %s\n", name);
#endif
		if(!bLoadImageFromFile(cont, name)) return 0;
	}
	if(cont->type == B_CONTENT_TYPE_IMAGE){
		width = cairo_image_surface_get_width(cont->imgsurf);
		height = cairo_image_surface_get_height(cont->imgsurf);
		cont->imgscale =
			((double)size) / (width > height ? width : height);
	}
	else if(cont->type == B_CONTENT_TYPE_IMAGE_SVG){
		if(stringEndsWith(name, "-symbolic", strlen(name))){
			cont->svgrecolor = 1;
		}
		cont->svgwidth = cont->svgheight = size;
	}
	return 1;
}

void bDestroyBoxChildren(bBoxWidget *box)
{
	bWidgetListItem *tmp, *next;

	for(tmp = box->children.first; tmp; tmp = next)
	{
		if(tmp->type == B_WIDGET_TYPE_BOX){
			bDestroyBoxRecursive(tmp->box);
		}
		else if(tmp->type == B_WIDGET_TYPE_CONTENT){
			freeCont(tmp->cont);
			free(tmp->cont);
		}
		next = tmp->next;
		free(tmp);
	}
	box->children.first = box->children.last = NULL;
}

void bDestroyBoxRecursive(bBoxWidget *box)
{
	bDestroyBoxChildren(box);
	free(box);
}

static unsigned char hex_digit(char digit, const char **err)
{
	if(digit >= '0' && digit <= '9')
		return digit - '0';
	else if(digit >= 'a' && digit <= 'f')
		return digit - 'a' + 10;
	else if(digit >= 'A' && digit <= 'F')
		return digit - 'A' + 10;
	else {
		if(digit){
			static char msg[] =
			"Hexadecimal digit must be in range [0-9], [a-f] or [A-F], found 'x'";
			if(digit)
			msg[sizeof(msg) - 3] = digit;
			*err = msg;
		}
		else {
			*err = "Found end of string instead of hex digit";
		}
		return 0;
	}
}

static unsigned char hex_byte(char high, char low, const char **err)
{
	unsigned char highval, lowval;
	highval = hex_digit(high, err);
	lowval = hex_digit(low, err);
	return (highval << 4) | lowval;
}

bWidgetColor bColorFromTheme(const char *name)
{
	struct bananui_theme *theme = loadTheme();
	double r, g, b, a;
	const char *err = NULL;
	char *combined_name = g_strdup_printf("%s:%s", bGlobalAppClass, name);
	char *color = themeSearchString(theme, combined_name, "Color");
	free(combined_name);
	if(!color){
		color = themeSearchString(theme, name, "Color");
	}
	if(!color){
		err = "not found";
		goto parse_error;
	}
	if(color[0] != '#'){
		err = "value must start with '#'";
		goto parse_error;
	}
	r = hex_byte(color[1], color[2], &err) / 255.0;
	if(err) goto parse_error;
	g = hex_byte(color[3], color[4], &err) / 255.0;
	if(err) goto parse_error;
	b = hex_byte(color[5], color[6], &err) / 255.0;
	if(err) goto parse_error;
	a = hex_byte(color[7], color[8], &err) / 255.0;
	free(color);
	if(!err){
		bWidgetColor color = bRGBA(r, g, b, a);
		return color;
	}
parse_error:
	fprintf(stderr, "Failed to parse theme color for %s: %s!\n", name, err);
	return bRGBA(0.0, 0.0, 0.0, 0.0);
}
