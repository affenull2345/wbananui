# For debian builds (sets the meson cross-file and multiarch directories)
-include options.mk

LIBDIR ?= /usr/lib

OBJECTS = window.o widget.o softkey.o xdg-shell-client-protocol.o \
	  text-input-client-protocol.o highlevel.o utils.o view.o simple.o
COMP_OBJECTS = comp/main.o \
	       comp/homescreen.o \
	       comp/textinput.o \
	       comp/deviceinfo.o \
	       comp/topbar.o \
	       comp/output.o \
	       comp/render.o \
	       comp/input.o \
	       comp/navigation.o \
	       comp/screenshot.o \
	       comp/wm.o
ALL_OBJECT_FILES = $(OBJECTS) $(COMP_OBJECTS)

APPS = about uitest clock fmradio

DEPS = wayland-client wayland-egl pangocairo \
       xkbcommon librsvg-2.0
COMP_DEPS = libgnome-menu-3.0 libcap

PKG_CONFIG = pkg-config
CFLAGS = -g -Wall -Wmissing-prototypes -Wmissing-declarations \
	-Wstrict-prototypes -Wformat-nonliteral -Wmissing-format-attribute \
	-O2 -Werror -Wno-error=unused-variable -Wno-error=unused-function
CPPFLAGS = -I. -Iinclude \
	$(shell $(PKG_CONFIG) --cflags $(DEPS))
LDFLAGS = $(shell $(PKG_CONFIG) --libs $(DEPS)) -ldl -lrt
COMP_CPPFLAGS = \
	-Iwlroots.obj/include -Iwlroots-$(WLROOTS_VERSION)/include \
	$(shell PKG_CONFIG_PATH="wlroots.obj/meson-private" \
		$(PKG_CONFIG) --cflags wlroots $(COMP_DEPS)) \
		-DWLR_USE_UNSTABLE \
		-DGMENU_I_KNOW_THIS_IS_UNSTABLE $(CPPFLAGS)
COMP_LDFLAGS = \
	-Lwlroots.obj \
	$(shell PKG_CONFIG_PATH="wlroots.obj/meson-private" \
		$(PKG_CONFIG) --libs --static wlroots) \
		$(shell $(PKG_CONFIG) --libs $(COMP_DEPS)) $(LDFLAGS)

AUTOLOGIN_CPPFLAGS =
AUTOLOGIN_LDFLAGS = -lpam -lpam_misc

WAYLAND_PROTOCOLS = $(shell $(PKG_CONFIG) \
		    --variable=pkgdatadir wayland-protocols)

OBJDIRS = wlroots.obj
WLROOTS_VERSION = 0.12.0

SO_VERSION = 0.1.0

DEPS_MK = .deps.mk

.PHONY: all
all: $(OBJDIRS) libwbananui.pc $(DEPS_MK)
	@$(MAKE) -C . all_no_deps
	@set -e; for app in $(APPS); do \
		echo "==> Building app $$app"; \
		$(MAKE) -f $(CURDIR)/Makefile.apps -C "apps/$$app"; \
	done

sinclude $(DEPS_MK)

.PHONY: all_no_deps
all_no_deps: libwbananui.so bananui-compositor bananui-autologin

libwbananui.pc: libwbananui.pc.in
	sed 's|@LIBDIR@|$(LIBDIR)|g' $< > $@

libwbananui.so: $(OBJECTS)
	$(CC) -Wl,-soname,$@.$(SO_VERSION) -shared -fPIC -o $@ $(OBJECTS) $(LDFLAGS)

bananui-autologin: autologin.o
	$(CC) -o $@ autologin.o $(AUTOLOGIN_LDFLAGS)

autologin.o: autologin.c
	$(CC) $(AUTOLOGIN_CPPFLAGS) -o $@ -c $< $(CFLAGS)

bananui-compositor: $(COMP_OBJECTS)
	$(CC) -o $@ $(COMP_OBJECTS) -L. -lwbananui $(COMP_LDFLAGS)

comp/%.o: comp/%.c xdg-shell-protocol.h
	$(CC) $(COMP_CPPFLAGS) -o $@ -c $< $(CFLAGS)

%.o: %.c
	$(CC) -fPIC $(CPPFLAGS) -o $@ -c $< $(CFLAGS)

text-input-client-protocol.c: text-input-client-protocol.h
	wayland-scanner private-code $(WAYLAND_PROTOCOLS)/unstable/text-input/text-input-unstable-v3.xml $@
text-input-client-protocol.h:
	wayland-scanner client-header $(WAYLAND_PROTOCOLS)/unstable/text-input/text-input-unstable-v3.xml $@
text-input-protocol.h:
	wayland-scanner server-header $(WAYLAND_PROTOCOLS)/unstable/text-input/text-input-unstable-v3.xml $@
xdg-shell-client-protocol.c: xdg-shell-client-protocol.h
	wayland-scanner private-code $(WAYLAND_PROTOCOLS)/stable/xdg-shell/xdg-shell.xml $@
xdg-shell-client-protocol.h:
	wayland-scanner client-header $(WAYLAND_PROTOCOLS)/stable/xdg-shell/xdg-shell.xml $@
xdg-shell-protocol.h:
	wayland-scanner server-header $(WAYLAND_PROTOCOLS)/stable/xdg-shell/xdg-shell.xml $@

ifneq ($(MAKECMDGOALS),all_no_deps)
ifneq ($(MAKECMDGOALS),clean)
ifneq ($(MAKECMDGOALS),cleanlibs)
.PHONY: .deps.mk
.deps.mk: xdg-shell-client-protocol.c text-input-client-protocol.c xdg-shell-protocol.h
	@printf '' > $@; set -e; for source in $(ALL_OBJECT_FILES:.o=); do \
		temp=$$(dirname $$source)/.$$(basename $$source).dep ; \
		$(CC) -MM -MG -MT $$source.o -o $$temp $(COMP_CPPFLAGS) \
			$$source.c; \
		cat $$temp >> $@; \
	done
endif
endif
endif

wlroots.obj: wlroots-$(WLROOTS_VERSION)/.extracted.stamp
	cd $(<D) && unset CFLAGS CPPFLAGS LDFLAGS && \
	  meson \
	    -Dexamples=false \
	    --default-library=static \
	    $(MESON_OPTIONS) ../$@
	ninja -C $@

wlroots-$(WLROOTS_VERSION)/.extracted.stamp: wlroots-$(WLROOTS_VERSION).tar.xz
	@echo "==> Extracting $(@D)"; xzcat $< | tar xf -
	@touch $@

.PHONY: clean
clean:
	rm -f $(OBJECTS) libwbananui.so bananui-compositor bananui-autologin libwbananui.pc xdg-shell-client-protocol.c xdg-shell-client-protocol.h xdg-shell-protocol.h text-input-client-protocol.c text-input-client-protocol.h $(COMP_OBJECTS)
	@set -e; for app in $(APPS); do \
		echo "==> Cleaning app $$app"; \
		$(MAKE) -f $(CURDIR)/Makefile.apps -C "apps/$$app" clean; \
	done

.PHONY: cleanlibs
cleanlibs:
	rm -rf $(OBJDIRS)

.PHONY: apps-install
apps-install:
	install -D -m 644 bananui-applications.menu $(DESTDIR)/etc/xdg/menus/bananui-applications.menu
	@set -e; for app in $(APPS); do \
		echo "==> Installing app $$app"; \
		$(MAKE) -f $(CURDIR)/Makefile.apps -C "apps/$$app" install; \
	done

.PHONY: compositor-install
compositor-install:
	install -D -m 755 bananui-compositor $(DESTDIR)/usr/bin/bananui-compositor
	install -D -m 644 71-seat-bananian.rules $(DESTDIR)/lib/udev/rules.d/71-seat-bananian.rules
	install -D -m 644 99-bananui-no-power-keys.conf $(DESTDIR)/etc/systemd/logind.conf.d/99-bananui-no-power-keys.conf

.PHONY: toolkit-install
toolkit-install:
	install -D -m 644 libwbananui.pc $(DESTDIR)$(LIBDIR)/pkgconfig/libwbananui.pc
	install -D -m 644 libwbananui.so $(DESTDIR)$(LIBDIR)/libwbananui.so.$(SO_VERSION)
	ln -f -s libwbananui.so.$(SO_VERSION) $(DESTDIR)$(LIBDIR)/libwbananui.so
	install -D -m 644 -t $(DESTDIR)/usr/include/bananui include/bananui/*
	install -D -m 644 -t $(DESTDIR)/usr/share/bananui/themes themes/*

.PHONY: autologin-install
autologin-install:
	install -D -m 755 bananui-autologin $(DESTDIR)/usr/bin/bananui-autologin

.PHONY: install
install: apps-install compositor-install toolkit-install autologin-install
